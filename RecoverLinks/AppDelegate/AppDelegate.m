//
//  AppDelegate.m
//  RecoverLinks
//
//  Created by  on 17/02/15.
//  Copyright (c) 2015 . All rights reserved.
//

#import "AppDelegate.h"
#import "ChartVC.h"
#import "HelpVC.h"
#import "ContactVC.h"
#import "LoginVC.h"
#import "WelcomeVC.h"
#import "TipOfTheDayVC.h"
#import "LearnVC.h"
#import "ProgCompleteVC.h"
#import "MessageListVC.h"

#import "Crittercism.h"


@interface AppDelegate ()<UIAlertViewDelegate>
{
    __weak IBOutlet UIButton *btnGraph;
    
    BOOL checkNetworkConnectionBool;

    UILocalNotification *remoteNotification;
    UIUserNotificationSettings *notificationSettings;
    NSMutableDictionary *notificationDict;
    UINavigationController *nav1;
    UINavigationController *nav2;
    UINavigationController *nav3;
    UINavigationController *nav4;
    UINavigationController *nav5;
}
@end

@implementation AppDelegate
@synthesize btnHome,btnLearn,btnSetting,btnContact,localnotification,tabBar;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
         [self.cstmTabBar setHidden:YES];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"WebKitDiskImageCacheEnabled"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    application.applicationIconBadgeNumber=0;
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(10, -40.f) forBarMetrics:UIBarMetricsDefault];
    
    tabBar.delegate = self;
    UILocalNotification *localNotification = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    if (localNotification) {
        application.applicationIconBadgeNumber = 0;
    }
    NSLog(@"notification ... - %@",localNotification);
    
    remoteNotification = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    notificationDict=[launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    
    self.window.rootViewController = [[UIViewController alloc]initWithNibName:nil bundle:nil];
    
    if (remoteNotification)
    {
        
        if([CommonFunctions reachabiltyCheck])
        {
            [CommonFunctions showActivityIndicatorWithText:@""];
            [self getDailySurvayStatus];
            
        }
        
    }
    
    else
    {
        if(![self isNotNull:[[NSUserDefaults standardUserDefaults] objectForKey:UD_PATIENT_ID]])
        {
            if ([[UIDevice currentDevice].systemVersion floatValue]>8.0) {
                UIUserNotificationType types = UIUserNotificationTypeSound |UIUserNotificationTypeBadge |UIUserNotificationTypeAlert;
                notificationSettings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
                [[UIApplication sharedApplication] registerUserNotificationSettings:notificationSettings];
            } else {
                [[UIApplication sharedApplication] registerForRemoteNotificationTypes:( UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert|UIRemoteNotificationTypeBadge)];
            }
            LoginVC *lvc=[[LoginVC alloc]init];
            self.navigationControl = [[UINavigationController alloc] initWithRootViewController:lvc];
            self.window.rootViewController = self.navigationControl;
            [self.window makeKeyAndVisible];
            [[UINavigationBar appearance]setShadowImage:[[UIImage alloc] init]];
        }
        else
        {
            if([CommonFunctions reachabiltyCheck])
            {
                [CommonFunctions showActivityIndicatorWithText:@""];
                [self getCurrentDay];
                
            }
            else
            {
                [CommonFunctions alertTitle:@"" withMessage:@"Check Your Network Connection" withDelegate:self];
                checkNetworkConnectionBool=YES;
            }
        }
        
    }
   return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}
- (void)applicationDidEnterBackground:(UIApplication *)application
{
    NSLog(@"App On Background");
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
}
- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}
- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

-(void)setUpTabBar
{
    UIView *statusView=[[UIView alloc]initWithFrame:CGRectMake(0, -[UIApplication sharedApplication].statusBarFrame.size.height,[UIApplication sharedApplication].statusBarFrame.size.width, [UIApplication sharedApplication].statusBarFrame.size.height)];
    [statusView setBackgroundColor:[UIColor colorWithRed:185.0f/255.0f green:-0/255.0f blue:36.0f/255.0f alpha:1]];
    [self.navigationControl.navigationBar addSubview:statusView];
    
    [[UINavigationBar appearance]setShadowImage:[[UIImage alloc] init]];
    
    [self.window setRootViewController:self.tabBar];
}



#pragma mark - GetCurrent DayAPI
-(void)getCurrentDay
{
    NSTimeZone *timeZone = [NSTimeZone localTimeZone];
    NSString *tzName = [timeZone name];
    
    NSString *eventID=[[NSUserDefaults standardUserDefaults]objectForKey:UD_EVENT_ID];
    NSString *url = [NSString stringWithFormat:@"getDay?Id=%@&timezone=%@",eventID,tzName];
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.152:100/api/Mobile/getDay?Id=2033&timezone=-3:00
    
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeGet withHttpHeaders:nil withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
        {
            [self tipOfTheDayView:responseDict];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo%@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:@"Could not connect to the server."];
                                          }
                                      }];
}

-(void)getDailySurvayStatus
{
    
    NSString *eventID=[[NSUserDefaults standardUserDefaults]objectForKey:UD_EVENT_ID];
    NSString *url = [NSString stringWithFormat:@"getDay/%@",eventID];
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.152:100/api/Mobile/getDay/
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeGet withHttpHeaders:nil withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
        {
            [self notificationSetup:responseDict];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo%@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:@"Could not connect to the server."];
                                          }
                                      }];
}

-(void)notificationSetup:(NSMutableDictionary*)dict
{
    NSString *str=[[[dict objectForKey:@"replyData"] objectForKey:@"SetDailySurvey"] stringValue];
    
    if([str isEqualToString:@"1"])
    {
        [[UIApplication sharedApplication]cancelAllLocalNotifications];
        [self registerForRemoteNotificationNextDay];
    }
    else
    {
        [self registerForRemoteNotification];
    }
    [[NSUserDefaults standardUserDefaults]setValue:[[dict objectForKey:@"replyData"]objectForKey:@"SetDailySurvey"] forKey:UD_SURVAY];
    [[NSUserDefaults standardUserDefaults]setValue:[[dict objectForKey:@"replyData"]objectForKey:@"Status"] forKey:UD_STATUS];
    [[NSUserDefaults standardUserDefaults]setValue:[[dict objectForKey:@"replyData"]objectForKey:@"dayNo"] forKey:UD_DAY];
    [[NSUserDefaults standardUserDefaults]synchronize];
    if([[[dict objectForKey:@"replyData"] objectForKey:@"dayNo"]intValue]<=30)
    {
      if ([[[dict objectForKey:@"replyData"]objectForKey:@"Status"]isEqualToString:@"Disenrolled"]||[[[dict objectForKey:@"replyData"]objectForKey:@"Status"]isEqualToString:@"Deceased"]||[[[dict objectForKey:@"replyData"]objectForKey:@"Status"]isEqualToString:@"Completed 30-days"])
      {
          
              [self allocTabBarAfter30Days];
            
            if ([[[notificationDict objectForKey:@"aps"] objectForKey:@"alert"]isEqualToString:@"You have new library material"])
            {
                [[UINavigationBar appearance]setShadowImage:[[UIImage alloc] init]];
                [APPDELEGATE.window addSubview:APPDELEGATE.cstmTabBar];
                self.window.rootViewController = self.tabBar;
                [APPDELEGATE.cstmTabBar setHidden:NO];
                [APPDELEGATE.window bringSubviewToFront:APPDELEGATE.cstmTabBar];
                [btnHome setBackgroundImage:[UIImage imageNamed:@"homeInactive"] forState:UIControlStateNormal];
                [btnLearn setBackgroundImage:[UIImage imageNamed:@"learnActive"] forState:UIControlStateNormal];
                [btnGraph setBackgroundImage:[UIImage imageNamed:@"graphInactive"] forState:UIControlStateNormal];
                [btnContact setBackgroundImage:[UIImage imageNamed:@"contactInactive"] forState:UIControlStateNormal];
                [btnSetting setBackgroundImage:[UIImage imageNamed:@"settingInactive"] forState:UIControlStateNormal];
                self.tabBar.selectedIndex=1;
                
                
            }
            else
            {
                [[UINavigationBar appearance]setShadowImage:[[UIImage alloc] init]];
                MessageListVC *mvc=[[MessageListVC alloc]initWithNibName:@"MessageListVC" bundle:nil];
                [APPDELEGATE.window addSubview:APPDELEGATE.cstmTabBar];
                
                self.window.rootViewController = self.tabBar;
                [APPDELEGATE.btnContact setBackgroundImage:[UIImage imageNamed:@"contactActive"] forState:UIControlStateNormal];
                [APPDELEGATE.btnHome setBackgroundImage:[UIImage imageNamed:@"homeInactive"] forState:UIControlStateNormal];
                self.tabBar.selectedIndex=3;
                [APPDELEGATE.window bringSubviewToFront:APPDELEGATE.cstmTabBar];
                [nav4 pushViewController:mvc animated:YES];
                
            }
        }
        else
        {
            if([[[[dict objectForKey:@"replyData"] objectForKey:@"SetDailySurvey"] stringValue] isEqualToString:@"1"])
            {
                
                TipOfTheDayVC *tv = [[TipOfTheDayVC alloc]initWithNibName:@"TipOfTheDayVC" bundle:nil];
                [self allocTabBarTipControllar:tv];
                
                if ([[[dict objectForKey:@"replyData"] objectForKey:@"typeOftip"] isEqualToString:@"Url"]) {
                    tv.videoUrl=[NSString stringWithFormat:@"%@",[[dict objectForKey:@"replyData"] objectForKey:@"Url"]];
                    NSLog(@"%@",tv.videoUrl);
                    tv.tipofDay=[NSString stringWithFormat:@"%@",[[dict objectForKey:@"replyData"] objectForKey:@"tipOfDay"]];
                }
                else
                {
                    if([self isNotNull:[[dict objectForKey:@"replyData"] objectForKey:@"tipOfDay"]])
                    {
                        tv.tipofDay=[NSString stringWithFormat:@"%@",[[dict objectForKey:@"replyData"] objectForKey:@"tipOfDay"] ];
                    }
                }
                
                if ([[[notificationDict objectForKey:@"aps"] objectForKey:@"alert"]isEqualToString:@"You have new library material"])
                {
                    [[UINavigationBar appearance]setShadowImage:[[UIImage alloc] init]];
                    [APPDELEGATE.window addSubview:APPDELEGATE.cstmTabBar];
                    self.window.rootViewController = self.tabBar;
                    [APPDELEGATE.cstmTabBar setHidden:NO];
                    [APPDELEGATE.window bringSubviewToFront:APPDELEGATE.cstmTabBar];
                    [btnHome setBackgroundImage:[UIImage imageNamed:@"homeInactive"] forState:UIControlStateNormal];
                    [btnLearn setBackgroundImage:[UIImage imageNamed:@"learnActive"] forState:UIControlStateNormal];
                    [btnGraph setBackgroundImage:[UIImage imageNamed:@"graphInactive"] forState:UIControlStateNormal];
                    [btnContact setBackgroundImage:[UIImage imageNamed:@"contactInactive"] forState:UIControlStateNormal];
                    [btnSetting setBackgroundImage:[UIImage imageNamed:@"settingInactive"] forState:UIControlStateNormal];
                    self.tabBar.selectedIndex=1;
                    
                }
                else
                {
                    [[UINavigationBar appearance]setShadowImage:[[UIImage alloc] init]];
                    MessageListVC *mvc=[[MessageListVC alloc]initWithNibName:@"MessageListVC" bundle:nil];
                    [APPDELEGATE.window addSubview:APPDELEGATE.cstmTabBar];
                    
                    self.window.rootViewController = self.tabBar;
                    [APPDELEGATE.btnContact setBackgroundImage:[UIImage imageNamed:@"contactActive"] forState:UIControlStateNormal];
                    [APPDELEGATE.btnHome setBackgroundImage:[UIImage imageNamed:@"homeInactive"] forState:UIControlStateNormal];
                    self.tabBar.selectedIndex=3;
                    [APPDELEGATE.window bringSubviewToFront:APPDELEGATE.cstmTabBar];
                    [nav4 pushViewController:mvc animated:YES];
                    
                }
                
            }
            else
            {
                [self allocTabBarControllar];
                if ([[[notificationDict objectForKey:@"aps"] objectForKey:@"alert"]isEqualToString:@"You have new library material"])
                {
                    [[UINavigationBar appearance]setShadowImage:[[UIImage alloc] init]];
                    [APPDELEGATE.window addSubview:APPDELEGATE.cstmTabBar];
                    self.window.rootViewController = self.tabBar;
                    [APPDELEGATE.cstmTabBar setHidden:NO];
                    [APPDELEGATE.window bringSubviewToFront:APPDELEGATE.cstmTabBar];
                    [btnHome setBackgroundImage:[UIImage imageNamed:@"homeInactive"] forState:UIControlStateNormal];
                    [btnLearn setBackgroundImage:[UIImage imageNamed:@"learnActive"] forState:UIControlStateNormal];
                    [btnGraph setBackgroundImage:[UIImage imageNamed:@"graphInactive"] forState:UIControlStateNormal];
                    [btnContact setBackgroundImage:[UIImage imageNamed:@"contactInactive"] forState:UIControlStateNormal];
                    [btnSetting setBackgroundImage:[UIImage imageNamed:@"settingInactive"] forState:UIControlStateNormal];
                    self.tabBar.selectedIndex=1;
                    
                    
                }
                else
                {
                    [[UINavigationBar appearance]setShadowImage:[[UIImage alloc] init]];
                    MessageListVC *mvc=[[MessageListVC alloc]initWithNibName:@"MessageListVC" bundle:nil];
                    [APPDELEGATE.window addSubview:APPDELEGATE.cstmTabBar];
                    
                    self.window.rootViewController = self.tabBar;
                    [APPDELEGATE.btnContact setBackgroundImage:[UIImage imageNamed:@"contactActive"] forState:UIControlStateNormal];
                    [APPDELEGATE.btnHome setBackgroundImage:[UIImage imageNamed:@"homeInactive"] forState:UIControlStateNormal];
                    self.tabBar.selectedIndex=3;
                    [APPDELEGATE.window bringSubviewToFront:APPDELEGATE.cstmTabBar];
                    [nav4 pushViewController:mvc animated:YES];
                    
                }
                
            }
        }
        
    }
    else
    {
        [self allocTabBarAfter30Days];
        
        
        if ([[[notificationDict objectForKey:@"aps"] objectForKey:@"alert"]isEqualToString:@"You have new library material"])
        {
            [[UINavigationBar appearance]setShadowImage:[[UIImage alloc] init]];
            [APPDELEGATE.window addSubview:APPDELEGATE.cstmTabBar];
            self.window.rootViewController = self.tabBar;
            [APPDELEGATE.cstmTabBar setHidden:NO];
            [APPDELEGATE.window bringSubviewToFront:APPDELEGATE.cstmTabBar];
            [btnHome setBackgroundImage:[UIImage imageNamed:@"homeInactive"] forState:UIControlStateNormal];
            [btnLearn setBackgroundImage:[UIImage imageNamed:@"learnActive"] forState:UIControlStateNormal];
            [btnGraph setBackgroundImage:[UIImage imageNamed:@"graphInactive"] forState:UIControlStateNormal];
            [btnContact setBackgroundImage:[UIImage imageNamed:@"contactInactive"] forState:UIControlStateNormal];
            [btnSetting setBackgroundImage:[UIImage imageNamed:@"settingInactive"] forState:UIControlStateNormal];
            self.tabBar.selectedIndex=1;
            
        }
        else
        {
            [[UINavigationBar appearance]setShadowImage:[[UIImage alloc] init]];
            MessageListVC *mvc=[[MessageListVC alloc]initWithNibName:@"MessageListVC" bundle:nil];
            [APPDELEGATE.window addSubview:APPDELEGATE.cstmTabBar];
            
            self.window.rootViewController = self.tabBar;
            [APPDELEGATE.btnContact setBackgroundImage:[UIImage imageNamed:@"contactActive"] forState:UIControlStateNormal];
            [APPDELEGATE.btnHome setBackgroundImage:[UIImage imageNamed:@"homeInactive"] forState:UIControlStateNormal];
            self.tabBar.selectedIndex=3;
            [APPDELEGATE.window bringSubviewToFront:APPDELEGATE.cstmTabBar];
            [nav4 pushViewController:mvc animated:YES];
            
        }
        
    }
    
    
}

-(void)tipOfTheDayView:(NSMutableDictionary*)dict
{
    NSLog(@"%@",[[dict objectForKey:@"replyData"] objectForKey:@"SetDailySurvey"]);
    NSString *str=[[[dict objectForKey:@"replyData"] objectForKey:@"SetDailySurvey"] stringValue];
    [[NSUserDefaults standardUserDefaults]setValue:[[dict objectForKey:@"replyData"]objectForKey:@"SetDailySurvey"] forKey:UD_SURVAY];
    if([str isEqualToString:@"1"])
    {
        [[UIApplication sharedApplication]cancelAllLocalNotifications];
        [self registerForRemoteNotificationNextDay];
    }
    else
    {
        [self registerForRemoteNotification];
    }
    
    
    NSLog(@"Schedule Notification : ----------%@",[[UIApplication sharedApplication] scheduledLocalNotifications]);
    [[NSUserDefaults standardUserDefaults]setValue:[[dict objectForKey:@"replyData"]objectForKey:@"dayNo"] forKey:UD_DAY];
    [[NSUserDefaults standardUserDefaults]setValue:[[dict objectForKey:@"replyData"]objectForKey:@"Status"] forKey:UD_STATUS];
    if([self isNotNull:[[dict objectForKey:@"replyData"]objectForKey:@"findmore"]])
    {
        [[NSUserDefaults standardUserDefaults]setValue:[[dict objectForKey:@"replyData"]objectForKey:@"findmore"] forKey:UD_FIND_MORE];
    }
    else
    {
        [[NSUserDefaults standardUserDefaults]setValue:[[dict objectForKey:@"replyData"]objectForKey:@"findmore"] forKey:UD_FIND_MORE]; 
    }

    [[NSUserDefaults standardUserDefaults]synchronize];
    if([[[dict objectForKey:@"replyData"] objectForKey:@"dayNo"]intValue]<=30)
    {
        if ([[[dict objectForKey:@"replyData"]objectForKey:@"Status"]isEqualToString:@"Disenrolled"]||[[[dict objectForKey:@"replyData"]objectForKey:@"Status"]isEqualToString:@"Deceased"]||[[[dict objectForKey:@"replyData"]objectForKey:@"Status"]isEqualToString:@"Completed 30-days"])
        {
            [self allocTabBarAfter30Days];
            
            APPDELEGATE.setUpTabBar;
            APPDELEGATE.tabBar.selectedIndex=0;
            [APPDELEGATE.window addSubview:APPDELEGATE.cstmTabBar];
            [APPDELEGATE.cstmTabBar setHidden:NO];
            [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
            [[UIApplication sharedApplication] cancelAllLocalNotifications];
            [[UIApplication sharedApplication]cancelLocalNotification:self.notification];
            [APPDELEGATE.btnHome setBackgroundImage:[UIImage imageNamed:@"homeActive"] forState:UIControlStateNormal];
   
        }
        else
        {
            if([[[[dict objectForKey:@"replyData"] objectForKey:@"SetDailySurvey"] stringValue] isEqualToString:@"1"])
            {
                
                TipOfTheDayVC *tv = [[TipOfTheDayVC alloc]initWithNibName:@"TipOfTheDayVC" bundle:nil];
                [self allocTabBarTipControllar:tv];
                
                if ([[[dict objectForKey:@"replyData"] objectForKey:@"typeOftip"] isEqualToString:@"Url"]) {
                    tv.videoUrl=[NSString stringWithFormat:@"%@",[[dict objectForKey:@"replyData"] objectForKey:@"Url"]];
                    NSLog(@"%@",tv.videoUrl);
                    tv.tipofDay=[NSString stringWithFormat:@"%@",[[dict objectForKey:@"replyData"] objectForKey:@"tipOfDay"]];
                }
                else
                {
                    if([self isNotNull:[[dict objectForKey:@"replyData"] objectForKey:@"tipOfDay"]])
                    {
                        tv.tipofDay=[NSString stringWithFormat:@"%@",[[dict objectForKey:@"replyData"] objectForKey:@"tipOfDay"] ];
                    }
                }
                
                [[UINavigationBar appearance]setShadowImage:[[UIImage alloc] init]];
                [self.cstmTabBar setHidden:NO];
                self.window.rootViewController = self.tabBar;
                
                [self.window addSubview:self.cstmTabBar];
                
            }
            else
            {
                [self allocTabBarControllar];
                APPDELEGATE.setUpTabBar;
                APPDELEGATE.tabBar.selectedIndex=0;
                [APPDELEGATE.window addSubview:APPDELEGATE.cstmTabBar];
                [APPDELEGATE.cstmTabBar setHidden:NO];
                
                [APPDELEGATE.btnHome setBackgroundImage:[UIImage imageNamed:@"homeActive"] forState:UIControlStateNormal];
            }
            

        }
    }
    else
    {
        [self allocTabBarAfter30Days];
        
        APPDELEGATE.setUpTabBar;
        APPDELEGATE.tabBar.selectedIndex=0;
        [APPDELEGATE.window addSubview:APPDELEGATE.cstmTabBar];
        [APPDELEGATE.cstmTabBar setHidden:NO];
        [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
        [[UIApplication sharedApplication] cancelAllLocalNotifications];
        [[UIApplication sharedApplication]cancelLocalNotification:self.notification];

        [APPDELEGATE.btnHome setBackgroundImage:[UIImage imageNamed:@"homeActive"] forState:UIControlStateNormal];
        
        
    }
    
}

- (void)registerForRemoteNotification {
    [[UIApplication sharedApplication]cancelAllLocalNotifications];
    if ([[UIDevice currentDevice].systemVersion floatValue]>8.0) {
        UIUserNotificationType types = UIUserNotificationTypeSound | UIUserNotificationTypeAlert|UIUserNotificationTypeBadge;
        notificationSettings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:notificationSettings];
    } else {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:( UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert|UIRemoteNotificationTypeBadge)];
    }
    
    NSDate *now = [NSDate date];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:now];
    [components setHour:11];
    // Gives us today's date but at 9am
    NSDate *next9am = [calendar dateFromComponents:components];
    if ([next9am timeIntervalSinceNow] < 0) {
        // If today's 9am already occurred, add 24hours to get to tomorrow's
        next9am = [next9am dateByAddingTimeInterval:60*60*24];
    }
    NSDictionary *dict=[[NSDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_DAY],@"day", nil];
    NSLog(@"Days Dict.... ------------- %@",dict);
    self.notification = [[UILocalNotification alloc] init];
    self.notification.fireDate = next9am;
    self.notification.timeZone=[NSTimeZone systemTimeZone];
    self.notification.userInfo=dict;
    self.notification.alertBody = @"You Have a New Survey Waiting.";
    self.notification.soundName=UILocalNotificationDefaultSoundName;
    // Set a repeat interval to daily
    self.notification.repeatInterval = NSDayCalendarUnit;

    [[UIApplication sharedApplication] scheduleLocalNotification:self.notification];

    
}

- (void)registerForRemoteNotificationNextDay {
    
    
    if ([[UIDevice currentDevice].systemVersion floatValue]>8.0) {
        UIUserNotificationType types = UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge;
        notificationSettings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:notificationSettings];
    } else {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert|UIRemoteNotificationTypeBadge)];
    }
    
    NSDate *now = [NSDate date];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:now];
    [components setHour:11];
    // Gives us today's date but at 9am
    NSDate *next9am = [calendar dateFromComponents:components];
    next9am = [next9am dateByAddingTimeInterval:60*60*24];
    
    NSDictionary *dict=[[NSDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_DAY],@"day", nil];
    NSLog(@"Days Dict.... ------------- %@",dict);
    self.notification = [[UILocalNotification alloc] init];
    self.notification.fireDate = next9am;
    self.notification.timeZone=[NSTimeZone systemTimeZone];
    self.notification.userInfo=dict;
    self.notification.alertBody = @"You Have a New Survey Waiting.";
    self.notification.soundName = UILocalNotificationDefaultSoundName;
    // Set a repeat interval to daily
    self.notification.repeatInterval = NSDayCalendarUnit;
   // notification.applicationIconBadgeNumber=[[UIApplication sharedApplication] applicationIconBadgeNumber]+1;
    [[UIApplication sharedApplication] scheduleLocalNotification:self.notification];
    NSLog(@"Nautification is : %@",[[UIApplication sharedApplication]scheduledLocalNotifications]);
}

#pragma mark- Remote Notification Methods
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSString *devToken = [[[[deviceToken description]
                            stringByReplacingOccurrencesOfString:@"<"withString:@""]
                           stringByReplacingOccurrencesOfString:@">" withString:@""]
                          stringByReplacingOccurrencesOfString: @" " withString: @""];
    NSLog(@"My token is: %@", devToken);
    pushDeviceToken=devToken;
}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo{
    

    if ( application.applicationState == UIApplicationStateInactive || application.applicationState == UIApplicationStateBackground  )
    {
        
        if ([[[userInfo objectForKey:@"aps"] objectForKey:@"alert"]isEqualToString:@"You have new library material"])
        {
            [APPDELEGATE.window addSubview:APPDELEGATE.cstmTabBar];
            [APPDELEGATE.cstmTabBar setHidden:NO];
            
            [btnHome setBackgroundImage:[UIImage imageNamed:@"homeInactive"] forState:UIControlStateNormal];
            [btnLearn setBackgroundImage:[UIImage imageNamed:@"learnActive"] forState:UIControlStateNormal];
            [btnGraph setBackgroundImage:[UIImage imageNamed:@"graphInactive"] forState:UIControlStateNormal];
            [btnContact setBackgroundImage:[UIImage imageNamed:@"contactInactive"] forState:UIControlStateNormal];
            [btnSetting setBackgroundImage:[UIImage imageNamed:@"settingInactive"] forState:UIControlStateNormal];
            self.tabBar.selectedIndex=1;
            
        }
        else
        {
            MessageListVC *mvc=[[MessageListVC alloc]initWithNibName:@"MessageListVC" bundle:nil];
            [APPDELEGATE.window addSubview:APPDELEGATE.cstmTabBar];
            [APPDELEGATE.cstmTabBar setHidden:NO];
            
            [APPDELEGATE.btnContact setBackgroundImage:[UIImage imageNamed:@"contactActive"] forState:UIControlStateNormal];
            [btnHome setBackgroundImage:[UIImage imageNamed:@"homeInactive"] forState:UIControlStateNormal];
            [btnLearn setBackgroundImage:[UIImage imageNamed:@"learnInactive"] forState:UIControlStateNormal];
            [btnGraph setBackgroundImage:[UIImage imageNamed:@"graphInactive"] forState:UIControlStateNormal];
            [btnSetting setBackgroundImage:[UIImage imageNamed:@"settingInactive"] forState:UIControlStateNormal];
            self.tabBar.selectedIndex=3;
            [nav4 pushViewController:mvc animated:YES];
        }
        
    }
    
    NSLog(@"%@",nav4.viewControllers);
    
}
- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    [CommonFunctions alertTitle:@"b" withMessage:[NSString stringWithFormat:@"%@",notification]];

}


- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    [application registerForRemoteNotifications];
}




- (IBAction)customeTabBarButtonClicked:(UIButton *)sender
{
    ContactVC *pvc = [[ContactVC alloc] initWithNibName:@"ContactVC" bundle:nil];
    nav4 = [[UINavigationController alloc] initWithRootViewController:pvc];

    self.tabBar.viewControllers = @[nav1, nav2, nav3, nav4,nav5];
    
    int btntag=(int)sender.tag;
    
    [btnHome setBackgroundImage:[UIImage imageNamed:@"homeInactive"] forState:UIControlStateNormal];
    [btnLearn setBackgroundImage:[UIImage imageNamed:@"learnInactive"] forState:UIControlStateNormal];
    [btnGraph setBackgroundImage:[UIImage imageNamed:@"graphInactive"] forState:UIControlStateNormal];
    [btnContact setBackgroundImage:[UIImage imageNamed:@"contactInactive"] forState:UIControlStateNormal];
    [btnSetting setBackgroundImage:[UIImage imageNamed:@"settingInactive"] forState:UIControlStateNormal];
    switch (btntag) {
        case 0:
            
            [btnHome setBackgroundImage:[UIImage imageNamed:@"homeActive"] forState:UIControlStateNormal];
            break;
        case 1:
            [btnLearn setBackgroundImage:[UIImage imageNamed:@"learnActive"] forState:UIControlStateNormal];
            break;
        case 2:
            [btnGraph setBackgroundImage:[UIImage imageNamed:@"graphActive"] forState:UIControlStateNormal];
            break;
        case 3:
            [btnContact setBackgroundImage:[UIImage imageNamed:@"contactActive"] forState:UIControlStateNormal];
            break;
        case 4:
            [btnSetting setBackgroundImage:[UIImage imageNamed:@"settingActive"] forState:UIControlStateNormal];
            break;
            
    }
    [APPDELEGATE.cstmTabBar setHidden:NO];
    
    [self.tabBar setSelectedIndex:btntag];
    
}
-(void)allocTabBarControllar
{
    self.tabBar=[[UITabBarController alloc]init];
    
    [self.cstmTabBar setHidden:NO];
    WelcomeVC *tvc = [[WelcomeVC alloc] initWithNibName:@"WelcomeVC" bundle:nil];
    nav1 = [[UINavigationController alloc] initWithRootViewController:tvc];
    
    LearnVC *evc = [[LearnVC alloc] initWithNibName:@"LearnVC" bundle:nil];
    nav2 = [[UINavigationController alloc] initWithRootViewController:evc];
    
    ChartVC *nvc = [[ChartVC alloc] initWithNibName:@"ChartVC" bundle:nil];
    nav3 = [[UINavigationController alloc] initWithRootViewController:nvc];
    
    ContactVC *pvc = [[ContactVC alloc] initWithNibName:@"ContactVC" bundle:nil];
    nav4 = [[UINavigationController alloc] initWithRootViewController:pvc];
    NSLog(@"------------------------------------------------------------------------------------------------%@",nav4.viewControllers);
    HelpVC *hvc = [[HelpVC alloc] initWithNibName:@"HelpVC" bundle:nil];
    nav5 = [[UINavigationController alloc] initWithRootViewController:hvc];
    
    
    self.tabBar.viewControllers = @[nav1, nav2, nav3, nav4,nav5];
    self.tabBar.delegate = self;
    
}

-(void)allocTabBarTipControllar:(UIViewController *)tvc
{
    self.tabBar=[[UITabBarController alloc]init];
    
    [self.cstmTabBar setHidden:NO];
    
    nav1 = [[UINavigationController alloc] initWithRootViewController:tvc];
    
    LearnVC *evc = [[LearnVC alloc] initWithNibName:@"LearnVC" bundle:nil];
    nav2 = [[UINavigationController alloc] initWithRootViewController:evc];
    
    ChartVC *nvc = [[ChartVC alloc] initWithNibName:@"ChartVC" bundle:nil];
    nav3 = [[UINavigationController alloc] initWithRootViewController:nvc];
    
    ContactVC *pvc = [[ContactVC alloc] initWithNibName:@"ContactVC" bundle:nil];
    nav4 = [[UINavigationController alloc] initWithRootViewController:pvc];
    
    HelpVC *hvc = [[HelpVC alloc] initWithNibName:@"HelpVC" bundle:nil];
    nav5 = [[UINavigationController alloc] initWithRootViewController:hvc];
    
    
    self.tabBar.viewControllers = @[nav1, nav2, nav3, nav4,nav5];
    self.tabBar.delegate = self;
}






-(void)allocTabBarAfter30Days
{
    self.tabBar=[[UITabBarController alloc]init];
    
    [self.cstmTabBar setHidden:NO];
    ProgCompleteVC *pcvc = [[ProgCompleteVC alloc] initWithNibName:@"ProgCompleteVC" bundle:nil];
    nav1 = [[UINavigationController alloc] initWithRootViewController:pcvc];
    
    LearnVC *evc = [[LearnVC alloc] initWithNibName:@"LearnVC" bundle:nil];
    nav2 = [[UINavigationController alloc] initWithRootViewController:evc];
    
    ChartVC *nvc = [[ChartVC alloc] initWithNibName:@"ChartVC" bundle:nil];
    nav3 = [[UINavigationController alloc] initWithRootViewController:nvc];
    
    ContactVC *pvc = [[ContactVC alloc] initWithNibName:@"ContactVC" bundle:nil];
    nav4 = [[UINavigationController alloc] initWithRootViewController:pvc];
    
    HelpVC *hvc = [[HelpVC alloc] initWithNibName:@"HelpVC" bundle:nil];
    nav5 = [[UINavigationController alloc] initWithRootViewController:hvc];
    
    self.tabBar.viewControllers = @[nav1, nav2, nav3, nav4,nav5];
    self.tabBar.delegate = self;
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(checkNetworkConnectionBool)
    {
        if([CommonFunctions reachabiltyCheck])
        {
            [CommonFunctions showActivityIndicatorWithText:@""];
            [self getCurrentDay];
            
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:@"Check Your Network Connection"  withDelegate:self];
            checkNetworkConnectionBool=YES;
        }
    }
}



@end