//
//  Config.h
//  Metal Calculator
//
//  Created by  on 26/11/14.
//  Copyright (c) 2014   Infosolutions Pvt Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Config : NSObject
extern  NSString		*serverURL;
#define IS_HEIGHT_GTE_568 [[UIScreen mainScreen ] bounds].size.height >= 568.0f
extern NSString		*serverHelpURL;
extern  NSString        *imageAgent;
extern NSString *pushDeviceToken;



@end
