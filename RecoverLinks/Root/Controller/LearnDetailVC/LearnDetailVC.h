//
//  LearnDetailVC.h
//  RecoverLinks
//
//  Created by  on 19/03/15.
//  Copyright (c) 2015 . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LearnDetailVC : UIViewController
{
    NSString *LearnURL;
    NSString *LearnVideoURL;
    
}
@property(strong,nonatomic) NSString *LearnURL;
@property(strong,nonatomic) NSString *LearnVideoURL;
@property(strong,nonatomic) NSString *previousView;
@end
