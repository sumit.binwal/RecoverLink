//
//  HelpVC.m
//  RecoverLinks
//
//  Created by  on 24/02/15.
//  Copyright (c) 2015 . All rights reserved.
//

#import "HelpVC.h"
#import "LoginVC.h"
@interface HelpVC ()<UIWebViewDelegate,UIAlertViewDelegate>
{
    
    IBOutlet UIWebView *wbView1;
    
    IBOutlet UIView *navView;
}
@end

@implementation HelpVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupView];
    // Do any additional setup after loading the view from its nib.
}
-(void)setupView
{
    [CommonFunctions setNavigationBar:self.navigationController];
    [self.navigationItem setTitle:@"SETTINGS"];
    
    [navView setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width,50)];
    [self.navigationController.navigationBar addSubview:navView];
    
    if([CommonFunctions reachabiltyCheck])
    {
        [CommonFunctions showActivityIndicatorWithText:@""];
        NSString *strEvntID=[[NSUserDefaults standardUserDefaults]valueForKey:UD_EVENT_ID];
        NSString *urlString=[NSString stringWithFormat:@"%@/Home/Content?EventId=%@&page=Settings",serverHelpURL,strEvntID];
        
        NSURL *linkUrl=[NSURL URLWithString:urlString];
        NSURLRequest *request= [NSURLRequest requestWithURL:linkUrl];
        [wbView1 loadRequest:request];
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection."];
    }
    
    NSLog(@"%@",APPDELEGATE.navigationHelp);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)logoutbuttonClicked
{
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [self setupView];
    [APPDELEGATE.cstmTabBar setHidden:NO];
    
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    [CommonFunctions removeActivityIndicator];
}

- (IBAction)logoutBtnClicked:(id)sender {
    if([CommonFunctions reachabiltyCheck])
    {
        [CommonFunctions showActivityIndicatorWithText:@""];
        [self logoutAPI];
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Check your internet connection first."];
    }
    
}



#pragma mark- WebView Delegate Methods
-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    [CommonFunctions removeActivityIndicator];
    
    CGSize contentSize = CGSizeMake([[webView stringByEvaluatingJavaScriptFromString:@"document.body.offsetWidth;"] floatValue],[[webView stringByEvaluatingJavaScriptFromString:@"document.body.offsetHeight;"] floatValue]);
    
    NSLog(@"--- content size : %@",NSStringFromCGSize(contentSize));
    CGRect frame = wbView1.frame;
    frame.size.height = 1;
    //wbView1.frame = frame;
    CGSize fittingSize = [wbView1 sizeThatFits:CGSizeZero];
    frame.size = fittingSize;
    //  wbView1.frame = frame;
    
    NSLog(@"size: %f, %f", fittingSize.width, fittingSize.height);
    
    if (contentSize.height>[[UIScreen mainScreen]bounds].size.height-150) {
        wbView1.scrollView.scrollEnabled=YES;
    }
    else
    {
        wbView1.scrollView.scrollEnabled=NO;
    }
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    // Ignore NSURLErrorDomain error -999.
    if (error.code == NSURLErrorCancelled) return;
    
    // Ignore "Fame Load Interrupted" errors. Seen after app store links.
    if (error.code == 102 && [error.domain isEqual:@"WebKitErrorDomain"]) return;
    
    [CommonFunctions alertTitle:@"" withMessage:@"Server Error" BtnTitle:@"Try Again" withDelegate:self];
    
    [CommonFunctions removeActivityIndicator];
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex==0)
    {
        if([CommonFunctions reachabiltyCheck])
        {
            [CommonFunctions showActivityIndicatorWithText:@""];
            NSString *strEvntID=[[NSUserDefaults standardUserDefaults]valueForKey:UD_EVENT_ID];
            NSString *urlString=[NSString stringWithFormat:@"%@/Home/Content?EventId=%@&page=Settings",serverHelpURL,strEvntID];
            
            NSURL *linkUrl=[NSURL URLWithString:urlString];
            NSURLRequest *request= [NSURLRequest requestWithURL:linkUrl];
            [wbView1 loadRequest:request];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection."];
        }
    }
}

#pragma mark- Web Service Calling API
-(void)logoutAPI
{
    NSMutableDictionary *params;
    NSString *url = [NSString stringWithFormat:@"logout"];
    params=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:UD_EVENT_ID],@"EventId",pushDeviceToken,@"DeviceId", nil];
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.152:100/api/Mobile/logout
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:nil withServiceName:url withParameters:params withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
        
        {
            
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:UD_FIRST_NAME];
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:UD_PATIENT_ID];
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:UD_EVENT_ID];
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:UD_FIND_MORE];
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:UD_STATUS];
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"vcName"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            LoginVC *lvc=[[LoginVC alloc]init];
            
            APPDELEGATE.navigationControl = [[UINavigationController alloc] initWithRootViewController:lvc];
            
            APPDELEGATE.window.rootViewController = APPDELEGATE.navigationControl;
            
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            [self.navigationController popViewControllerAnimated:NO];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo%@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              
                                              
                                          }
                                          else{
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:@"Could not connect to the server."];
                                          }
                                          
                                      }];
}

@end
