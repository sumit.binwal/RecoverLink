//
//  LearnVC.m
//  RecoverLinks
//
//  Created by  on 24/02/15.
//  Copyright (c) 2015 . All rights reserved.
//
#import "YTPlayerView.h"
#import "LearnVC.h"
#import "LearnCustomeCell.h"
#import "LearnDetailVC.h"
@interface LearnVC ()<UITabBarControllerDelegate,UIWebViewDelegate,YTPlayerViewDelegate>
{
    IBOutlet UICollectionView *collectionview;
    NSMutableArray *articleArr;
    IBOutlet UICollectionViewFlowLayout *collectionViewFlowLayout;
    UIRefreshControl *refreshControl;
}
@end

@implementation LearnVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tabBarController.delegate =self;
    
    [self setupView];
    // Do any additional setup after loading the view from its nib.
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    }
}
-(void)setupView
{
    [CommonFunctions setNavigationBar:self.navigationController];
        [self.navigationItem setTitle:@"LEARN"];
    
    [collectionview registerNib:[UINib nibWithNibName:@"LearnCustomeCell" bundle:nil] forCellWithReuseIdentifier:@"LearnCustomeCell"];

    collectionViewFlowLayout = (UICollectionViewFlowLayout*)collectionview.collectionViewLayout;
    collectionViewFlowLayout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    if ([CommonFunctions reachabiltyCheck]) {
        
        [CommonFunctions showActivityIndicatorWithText:@""];
            [self getArticleList];
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Check your network connection."];
    }

    refreshControl = [[UIRefreshControl alloc]
                                        init];
    refreshControl.tintColor = [UIColor blueColor];
    [refreshControl addTarget:self action:@selector(changeSorting) forControlEvents:UIControlEventValueChanged];
    [collectionview addSubview:refreshControl];
}

-(void)changeSorting
{
if([CommonFunctions reachabiltyCheck])
{
    [self getArticleList];
    [self performSelector:@selector(updateTable) withObject:nil
               afterDelay:1];
}
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Check your network connection."];
    }
    
}

- (void)updateTable
{
    [refreshControl endRefreshing];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [APPDELEGATE.cstmTabBar setHidden:NO];
    [APPDELEGATE.btnLearn setBackgroundImage:[UIImage imageNamed:@"learnActive"] forState:UIControlStateNormal];
    
    if ([CommonFunctions reachabiltyCheck]) {
        
        [CommonFunctions showActivityIndicatorWithText:@""];
        [self getArticleList];
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Check your network connection."];
    }
}

#pragma mark-UICollectionView Delegate Methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return articleArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    LearnCustomeCell *cell = (LearnCustomeCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"LearnCustomeCell" forIndexPath:indexPath];
   
    NSArray *paths = [collectionView indexPathsForVisibleItems];
    
    if (articleArr.count == paths.count) {
        collectionView.scrollEnabled=NO;
    }
    else
    {
        collectionView.scrollEnabled=YES;
    }

    
    if([[[articleArr objectAtIndex:indexPath.row] objectForKey:@"Type"] isEqualToString:@"Youtube"])
    {
        [cell.imgVwLearn setHidden:YES];
        [cell.imgPlayBtn setHidden:YES];
        [cell.viewYTPlayer setHidden:NO];
        [cell.viewYTPlayer setDelegate:self];
//        cell.wbView.scrollView.scrollEnabled =NO;
//        NSLog(@"%@",[[articleArr objectAtIndex:indexPath.row]objectForKey:@"url"]);
        NSString *youTubeID = [self extractYoutubeID:[[articleArr objectAtIndex:indexPath.row]objectForKey:@"url"]];   // url is youtube url
        
        NSDictionary *playerVars = @{
                                     @"modestbranding":@1,@"showinfo": @0,
                                     @"controls":@0,@"rel":@0
                                     
                                     };
        //[webView loadWithVideoId:@"M7lc1UVf-VE" playerVars:playerVars];
        [cell.viewYTPlayer loadWithVideoId:youTubeID playerVars:playerVars];
       
//        NSString *embedHTML=[NSString stringWithFormat:@"\
//                             <html><head>\
//                             <script type=\"text/javascript\">\
//                             var tag = document.createElement('script');\
//                             tag.src ='http://www.youtube.com/player_api';\
//                             var firstScriptTag = document.getElementsByTagName('script')[0];\
//                             firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);\
//                             var player;\
//                             function onYouTubePlayerAPIReady() {\
//                             player = new YT.Player('player', {\
//                             height: '%f',\
//                             width: '%f',\
//                             videoId: '%@?showinfo=0&modestbranding=1',\
//                             events: {\
//                             'onReady': onPlayerReady,\
//                             'onStateChange': onPlayerStateChange\
//                             }\
//                             });\
//                             }\
//                             function onPlayerReady(event) {\
//                             }\
//                             var done = false;\
//                             function onPlayerStateChange(event) {\
//                             if (event.data == YT.PlayerState.PLAYING && !done) {\
//                             window.location = \"start:anything\";\
//                             }\
//                             if (event.data == YT.PlayerState.BUFFERING) {\
//                             window.location = \"startt:anything\";\
//                             };\
//                             if (event.data == YT.PlayerState.ENDED) {\
//                             window.location = \"callback:anything\";\
//                             };\
//                             }\
//                             function stopVideo() {\
//                             player.stopVideo();\
//                             }\
//                             </script></head><body style=\"margin:0;\">\
//                             <div id='player'></div></body></html>",cell.wbView.frame.size.height,cell.wbView.frame.size.width,youTubeID];
//        //         build the path where you're going to save the HTML
//        
//        NSString *docsFolder = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
//        NSString *filename = [docsFolder stringByAppendingPathComponent:@"youtube.html"];
//        
//        // save the NSString that contains the HTML to a file
//        
//        NSError *error;
//        [embedHTML writeToFile:filename atomically:NO encoding:NSUTF8StringEncoding error:&error];
//        
//        NSURL *url = [NSURL fileURLWithPath:filename];
//        NSURLRequest *request = [NSURLRequest requestWithURL:url];
//        [cell.wbView loadRequest:request];
        [cell.lblArticleLabel setText:[[articleArr objectAtIndex:indexPath.row] objectForKey:@"DocumentType"]];
    }
    else
    {
        [cell.viewYTPlayer setHidden:YES];
        [cell.imgPlayBtn setHidden:YES];
        [cell.imgVwLearn setHidden:NO];
        NSURL *imgUrl=[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[articleArr objectAtIndex:indexPath.row]objectForKey:@"icon"]]];
        [cell.imgVwLearn sd_setImageWithURL:imgUrl placeholderImage:nil options:SDWebImageProgressiveDownload completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            [cell.imgVwLearn layoutIfNeeded];
            NSLog(@"done");
        }];
//        [cell.imgVwLearn setImageWithURL:imgUrl usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [cell.lblArticleLabel setText:[[articleArr objectAtIndex:indexPath.row] objectForKey:@"DocumentType"]];
    }

    
    return cell;
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
   // float width=[UIScreen mainScreen].bounds.size.width/3-.05;
    
    if([UIScreen mainScreen].bounds.size.width==375)
    {
    return CGSizeMake(124.25f , 147.0f);
    }
    else if ([UIScreen mainScreen].bounds.size.width==414)
    {
    return CGSizeMake(137.25f , 147.0f);
    }
    else
    {
            return CGSizeMake(106.0f , 147.0f);
    }
    
    //return CGSizeMake(width, 147.0f);
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    if([CommonFunctions reachabiltyCheck])
    {
        [self trackDeviceDocumentActivityAPI:[[articleArr objectAtIndex:indexPath.row] objectForKey:@"ArticlesId"]];
        LearnDetailVC *ldvc=[[LearnDetailVC alloc]initWithNibName:@"LearnDetailVC" bundle:nil];
        ldvc.previousView=@"learn";
        if([[[articleArr objectAtIndex:indexPath.row] objectForKey:@"Type"] isEqualToString:@"Youtube"])
        {
            
        }
        else
        {
            ldvc.LearnURL=[[articleArr objectAtIndex:indexPath.row]objectForKey:@"url"];
            [self.navigationController pushViewController:ldvc animated:YES];
        }
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection."];
    }

}


- (NSString *)extractYoutubeID:(NSString *)youtubeURL
{
    //NSLog(@"youtube  %@",youtubeURL);
    NSError *error = NULL;
    NSString *regexString = @"(?<=v(=|/))([-a-zA-Z0-9_]+)|(?<=youtu.be/)([-a-zA-Z0-9_]+)";
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:regexString options:NSRegularExpressionCaseInsensitive error:&error];
    NSRange rangeOfFirstMatch = [regex rangeOfFirstMatchInString:youtubeURL options:0 range:NSMakeRange(0, [youtubeURL length])];
    if(!NSEqualRanges(rangeOfFirstMatch, NSMakeRange(NSNotFound, 0)))
    {
        NSString *substringForFirstMatch = [youtubeURL substringWithRange:rangeOfFirstMatch];
        return substringForFirstMatch;
    }
    return nil;
}
#pragma mark Collection view layout things
// Layout: Set cell size

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0f;
}

// Layout: Set Edges
- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    // return UIEdgeInsetsMake(0,8,0,8);  // top, left, bottom, right
    return UIEdgeInsetsMake(0,1,0,1);  // top, left, bottom, right
}


#pragma mark- WebServices API..

-(void)getArticleList
{
    
    NSString *url = [NSString stringWithFormat:@"Articles/%@",[[NSUserDefaults standardUserDefaults] objectForKey:UD_EVENT_ID]];
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.152:100/api/Mobile/Articles/1
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeGet withHttpHeaders:nil withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {
            articleArr =[[NSMutableArray alloc]init];
            [articleArr addObjectsFromArray:[responseDict objectForKey:@"replyData"]];
            
//            for (int i=0; i<articleArr.count; i++)
//            {
//                if ([articleArr[i][@"Type"] isEqual:@"Youtube"])
//                {
//                    NSString *str = articleArr[i][@"url"];
//                    str = [str stringByReplacingOccurrencesOfString:@"embed" withString:@"watch"];
//                    articleArr[i][@"url"] = str;
//                }
//            }
            
            [collectionview reloadData];
            NSLog(@"------------- %lu",(unsigned long)articleArr.count);
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            [self.navigationController popViewControllerAnimated:NO];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo%@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              
                                              
                                          }
                                          else{
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:@"Could not connect to the server."];
                                          }
                                          
                                      }];
}


-(void)trackDeviceDocumentActivityAPI:(NSString *)docId
{
    NSTimeZone *timeZone = [NSTimeZone localTimeZone];
    NSString *tzName = [timeZone name];
    
    NSString *eventID=[[NSUserDefaults standardUserDefaults]objectForKey:UD_EVENT_ID];
   // {"TractId":"5","EventId":"1005","DocumentID":"4080","timezone":"America/New_York"}
    NSMutableDictionary *params=[[NSMutableDictionary alloc]initWithObjectsAndKeys:@"5",@"TractId",eventID,@"EventId",tzName,@"timezone",docId,@"DocumentID", nil];
        
    NSString *url = [NSString stringWithFormat:@"DeviceDocumentclick"];
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    //http://192.168.0.152:99/api/Mobile/DeviceDocumentclick
    
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:nil withServiceName:url withParameters:params withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSMutableDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
       // [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
        {
            NSLog(@"%@",responseDict);
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo%@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:@"Could not connect to the server."];
                                          }
                                      }];
}

#pragma mark- WebView Delegate Methods
- (void)playerView:(YTPlayerView *)playerView didChangeToState:(YTPlayerState)state {
    switch (state) {
        case kYTPlayerStatePlaying:
            
        {NSLog(@"Started playback");
            CGPoint buttonPosition = [playerView convertPoint:CGPointZero toView:collectionview];
            NSIndexPath *indexPath = [collectionview indexPathForItemAtPoint:buttonPosition];
            
            NSLog(@"%ld", (long)indexPath.row);
//            NSArray *selectedIndexPath = [collectionview indexPathsForSelectedItems];
            NSLog(@"%@",indexPath);
//    NSIndexPath *centerCellIndexPath =
//       [collectionview indexPathForItemAtPoint:[self.view convertPoint:[self.view center] toView:collectionview]];
//            [collectionview indexPathForItemAtPoint:iindexPath];
            [self trackDeviceDocumentActivityAPI:[[articleArr objectAtIndex:indexPath.row] objectForKey:@"ArticlesId"]];

        }
            
            break;
        case kYTPlayerStatePaused:
            NSLog(@"Paused playback");
            break;
        default:
            break;
    }
}
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
    NSIndexPath *centerCellIndexPath =
    [collectionview indexPathForItemAtPoint:
     [self.view convertPoint:[self.view center] toView:collectionview]];

 if ( [[[request URL] scheme] isEqualToString:@"start"] ) {

     [self trackDeviceDocumentActivityAPI:[[articleArr objectAtIndex:centerCellIndexPath.row] objectForKey:@"ArticlesId"]];

        return YES;
    }
    return YES;
}


@end
