//
//  BarGraphVC.m
//  RecoverLinks
//
//  Created by  on 09/04/15.
//  Copyright (c) 2015 . All rights reserved.
//

#import "BarGraphVC.h"

@interface BarGraphVC ()<graphDelegate>
{
    
    IBOutlet UIView *containerVw;
    IBOutlet PCLineChartView *objectView;
    IBOutlet PCLineChartView *chartView;
    IBOutlet UILabel *lblZone;
    IBOutlet UILabel *lblYAxis;
    NSArray *graphDataArr;
    NSMutableArray *filteredArray;
    IBOutlet UIImageView *zoneBox;
}
@end

@implementation BarGraphVC
@synthesize lineChartView;
-(void)addImageView:(CGRect)rect
{
    UIImageView *imgVw=[[UIImageView alloc]initWithFrame:rect];
    [imgVw setImage:[UIImage imageNamed:@"cross"]];
    [lineChartView addSubview:imgVw];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [CommonFunctions setNavigationBar:self.navigationController];
    [self.navigationItem setTitle:@"CHART"];
  

    lblZone.text=@"";
    lblWeight.text=@"";
    
    [self.view addSubview:lineChartView];
    lblYAxis.transform = CGAffineTransformMakeRotation (-M_PI/2);
    
}

-(void)viewWillAppear:(BOOL)animated
{
    if([CommonFunctions reachabiltyCheck])
    {
        [CommonFunctions showActivityIndicatorWithText:@""];
        [self getLineGraphData];
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection"];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- WebService Calling Methods
-(void)getLineGraphData
{
    NSString *url = [NSString stringWithFormat:@"QuestionsGraph?Id=%@&QuestionId=2",[[NSUserDefaults standardUserDefaults] objectForKey:UD_EVENT_ID]];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://recoverlink.reliablesandbox.net/api/Mobile/QuestionsGraph?Id=6&QuestionId=2
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeGet withHttpHeaders:nil withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);

        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {
            

            
            
            [self performSelectorOnMainThread:@selector(fetchGraphData:) withObject:responseDict waitUntilDone:YES];
            
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo%@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:@"Could not connect to the server."];
                                          }
                                      }];
}
-(void)fetchGraphData:(NSDictionary *)dict
{
    
    if([UIScreen mainScreen].bounds.size.width==414)
    {
    lineChartView=[[PCLineChartView alloc]initWithFrame:CGRectMake([UIScreen mainScreen].bounds.origin.x+60, objectView.frame.origin.y-20, objectView.frame.size.width+10, objectView.frame.size.height+17)];
    }
    else if([UIScreen mainScreen].bounds.size.width==375)
    {
    lineChartView=[[PCLineChartView alloc]initWithFrame:CGRectMake([UIScreen mainScreen].bounds.origin.x+45, objectView.frame.origin.y-15, objectView.frame.size.width, objectView.frame.size.height+17)];
    }
    else
    {
    lineChartView=[[PCLineChartView alloc]initWithFrame:CGRectMake([UIScreen mainScreen].bounds.origin.x+10, objectView.frame.origin.y-20, objectView.frame.size.width+20, objectView.frame.size.height+17)];
    }
    lineChartView.maxValue=7;
    lineChartView.minValue=-7;
    lineChartView.delegate=self;
    
    [self.view addSubview:lineChartView];
    
    filteredArray=[[NSMutableArray alloc]init];
    graphDataArr=[dict objectForKey:@"replyData"];

    for (int i=-0; i<30; i++)

        {
           int actualValue;
            if(i<graphDataArr.count)
            {
            
                if ([[[graphDataArr objectAtIndex:i] objectForKey:@"Value"] isEqualToString:@"absent"])
                {
                    actualValue=-8;
                    //-8 Value For Indicates the today is absent
                }
                else
                {
                    int rgistrationValue=[[[graphDataArr objectAtIndex:0]objectForKey:@"Value"] intValue];
                    
                   int actualValue1=[[[graphDataArr objectAtIndex:i]objectForKey:@"Value"]intValue]-rgistrationValue;
                    
                        actualValue=actualValue1;

                }
            }
            else
            {
                actualValue=-9;
               //-9 Value For Indicates that remaining days;
            }
        
 [filteredArray addObject:[NSString stringWithFormat:@"%d",actualValue]];

    }
  
    
    if([[[NSUserDefaults standardUserDefaults]objectForKey:UD_DAY]intValue]<=0)
    {
    lblWeight.text=@"";
    lblZone.text=@"";
    }
    else
    {
    if([[[NSUserDefaults standardUserDefaults]objectForKey:UD_DAY]intValue] > 30)
    {
        if([[filteredArray objectAtIndex:graphDataArr.count-2] intValue]>0)
        {
    lblWeight.text=[NSString stringWithFormat:@"+%@",[filteredArray objectAtIndex:graphDataArr.count-2]];
        }
        else
        {
    lblWeight.text=[NSString stringWithFormat:@"%@",[filteredArray objectAtIndex:graphDataArr.count-2]];
        }
    }
       else if([[[NSUserDefaults standardUserDefaults]objectForKey:UD_DAY]intValue] == 30)
        {
            if([[filteredArray objectAtIndex:graphDataArr.count-2] intValue]>0)
            {
                lblWeight.text=[NSString stringWithFormat:@"+%@",[filteredArray objectAtIndex:graphDataArr.count-2]];
            }
            else
            {
                lblWeight.text=[NSString stringWithFormat:@"%@",[filteredArray objectAtIndex:graphDataArr.count-2]];
            }

        
        }
    else
    {
        NSLog(@"--%@",lblWeight.text);

       // if (graphDataArr.count>1) {
            if([[filteredArray objectAtIndex:graphDataArr.count-1] intValue]>0)
            {
                lblWeight.text=[NSString stringWithFormat:@"+%@",[filteredArray objectAtIndex:graphDataArr.count-1]];
            }
            else
            {
                lblWeight.text=[NSString stringWithFormat:@"%@",[filteredArray objectAtIndex:graphDataArr.count-1]];
            }
       // }
    }
    }
    if ([lblWeight.text isEqualToString:@"+7"]||[lblWeight.text isEqualToString:@"+5"]||[lblWeight.text isEqualToString:@"+6"])
    {
        lblZone.text=@"ALERT";
        [zoneBox setBackgroundColor:[UIColor colorWithRed:212.0f/255.0f green:150.0f/255.0f blue:162.0f/255.0f alpha:1]];
    }
    else if ([lblWeight.text isEqualToString:@"+4"]||[lblWeight.text isEqualToString:@"+3"]||[lblWeight.text isEqualToString:@"-5"]||[lblWeight.text isEqualToString:@"-6"]||[lblWeight.text isEqualToString:@"-7"]) {
        lblZone.text=@"ALERT";
        [zoneBox setBackgroundColor:[UIColor colorWithRed:234.0f/255.0f green:232.0f/255.0f blue:156.0f/255.0f alpha:1]];
    }
    else if ([lblWeight.text isEqualToString:@"-8"]||[lblWeight.text isEqualToString:@"-9"]||[lblWeight.text isEqualToString:@""])
    {
        lblZone.text=@"";
        lblWeight.text=@"";
        [zoneBox setBackgroundColor:[UIColor clearColor]];
    }
    else
    {
        if(graphDataArr.count>1)
        {
         lblZone.text=@"OK";

        }
        else
        {
         lblZone.text=@"";
        }
        [zoneBox setBackgroundColor:[UIColor colorWithRed:200.0f/255.0f green:226.0f/255.0f blue:166.0f/255.0f alpha:1]];
    }
    
    if([lblWeight.text intValue]>7)
    {
        lblZone.text=@"ALERT";
        [zoneBox setBackgroundColor:[UIColor colorWithRed:212.0f/255.0f green:150.0f/255.0f blue:162.0f/255.0f alpha:1]];
    }
    else if ([lblWeight.text intValue]<-7)
    {
        lblZone.text=@"ALERT";
        [zoneBox setBackgroundColor:[UIColor colorWithRed:234.0f/255.0f green:232.0f/255.0f blue:156.0f/255.0f alpha:1]];

    }
    
    NSMutableArray *xValue=[[NSMutableArray alloc]init];

    NSMutableArray *components=[[NSMutableArray alloc]init];
    
    for (int i=0; i<=31; i++) {
        [xValue addObject:[NSString stringWithFormat:@"%d",i]];
    }
    
    PCLineChartViewComponent *component = [[PCLineChartViewComponent alloc] init];
    [component setPoints:filteredArray];
    [component setShouldLabelValues:NO];
    [component setColour:PCColorLineBlack];
    
    [components addObject:component];

    [lineChartView setComponents:components];
    [lineChartView setXLabels:xValue];
}

@end
