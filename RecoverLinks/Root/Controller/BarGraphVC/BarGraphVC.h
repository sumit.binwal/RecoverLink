//
//  BarGraphVC.h
//  RecoverLinks
//
//  Created by  on 09/04/15.
//  Copyright (c) 2015 . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PCLineChartView.h"
@interface BarGraphVC : UIViewController
{

    IBOutlet UILabel *lblWeight;
}
@property (nonatomic, strong)  PCLineChartView *lineChartView;
@end
