//
//  RegistrationVC.m
//  SampleProject
//
//  Created by  on 17/02/15.
//  Copyright (c) 2015 ; Infosolutions Pvt Ltd. All rights reserved.
//

#import "RegistrationVC.h"
#import "WelcomeVC.h"
@interface RegistrationVC ()<UITextFieldDelegate,UIAlertViewDelegate,UIGestureRecognizerDelegate,UIPickerViewDelegate,UIPickerViewDataSource>
{
    UITextField *tfActiveTxt;
    UITextField *tfActiveTxt1;
    IBOutlet UITextField *txtLastName;
    IBOutlet UITextField *txtFirstName;
    IBOutlet UITextField *txtHospitalPassword;
    IBOutlet UITextField *txtHospitalUsername;
    IBOutlet UITextField *txtSelectHospital;
    IBOutlet UITextField *txtConfirmPassword;
    IBOutlet UITextField *txtPassword;
    IBOutlet UITextField *txtUserName;
    IBOutlet UITextField *txtDryWeight;
    IBOutlet UITextField *txtUniqueID;
    IBOutlet UIScrollView *scrllVw;
    IBOutlet UIPickerView *slectHospitalPicker;
    NSMutableArray *arrHospital;
    NSString *pickerValue;
    NSString *hospitalID;
    IBOutlet UIView *viewPIcker;
    IBOutlet UIBarButtonItem *cancleButton;
    IBOutlet UIView *toolBarView;
    IBOutlet UIBarButtonItem *nxtButton;
    //Flag For ScrollVw In TextFieldDidBeginEditing
    BOOL flagforScrollVwTxtDidBeginEditng;
    BOOL flagforNoInternetAccess;
    //Flag For Casemanger Verified or Not..
        BOOL flagForCMValidation;
    //Flag For server error..
    BOOL flagFroCouldNotConnectToServer;
    //Flag For MRN Verified or Not..
    BOOL flagForMRNValidation;
    
}
@end

@implementation RegistrationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"push is %@",pushDeviceToken);
    
    [self setupView];
}
-(void)setupView
{

    [CommonFunctions setNavigationBar:self.navigationController];

    //------Attributed Label In Navigation Bar..
    UILabel *titleLabel = [[UILabel alloc]init];
    
    
    NSDictionary *attributes2 = @{NSForegroundColorAttributeName: [UIColor whiteColor],
                                  NSFontAttributeName: [UIFont fontWithName:FONT_SEGOE_BOLD size:20.0]
                                  };
    
    NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:@"REGISTRATION"];
    [string addAttributes:attributes2 range:NSMakeRange(0,12)];
    titleLabel.attributedText=string;
    [titleLabel sizeToFit];
    self.navigationItem.titleView = titleLabel;
    
    //--Attributed Label In Navigation Bar End ..
    
    //-- Left Padding Space to Text Field
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 21, 0)];
    txtSelectHospital.leftView = paddingView;
    txtSelectHospital.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *paddingView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 21, 0)];
    txtConfirmPassword.leftView = paddingView1;
    txtConfirmPassword.leftViewMode = UITextFieldViewModeAlways;

    UIView *paddingView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 21, 0)];
    txtDryWeight.leftView = paddingView2;
    txtDryWeight.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *paddingView3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 21, 0)];
    txtPassword.leftView = paddingView3;
    txtPassword.leftViewMode = UITextFieldViewModeAlways;

    UIView *paddingView4 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 21, 0)];
    txtUniqueID.leftView = paddingView4;
    txtUniqueID.leftViewMode = UITextFieldViewModeAlways;

    UIView *paddingView5 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 21, 0)];
    txtUserName.leftView = paddingView5;
    txtUserName.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *paddingView6 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 21, 0)];
    txtHospitalUsername.leftView = paddingView6;
    txtHospitalUsername.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *paddingView7 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 21, 0)];
    txtHospitalPassword.leftView = paddingView7;
    txtHospitalPassword.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *paddingView8 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 21, 0)];
    txtFirstName.leftView = paddingView8;
    txtFirstName.leftViewMode = UITextFieldViewModeAlways;
    
    
    UIView *paddingView9 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 21, 0)];
    txtLastName.leftView = paddingView9;
    txtLastName.leftViewMode = UITextFieldViewModeAlways;
    
    
    //------ TextField Placeholder Color Change Code Start -------//
    [txtPassword setValue:[UIColor colorWithRed:110.0f/255.0f green:163.0f/255.0f blue:173.0f/255.0f alpha:1]
               forKeyPath:@"_placeholderLabel.textColor"];
    [txtUserName setValue:[UIColor colorWithRed:110.0f/255.0f green:163.0f/255.0f blue:173.0f/255.0f alpha:1]
               forKeyPath:@"_placeholderLabel.textColor"];
    [txtUniqueID setValue:[UIColor colorWithRed:110.0f/255.0f green:163.0f/255.0f blue:173.0f/255.0f alpha:1]
               forKeyPath:@"_placeholderLabel.textColor"];
    [txtSelectHospital setValue:[UIColor colorWithRed:110.0f/255.0f green:163.0f/255.0f blue:173.0f/255.0f alpha:1]
               forKeyPath:@"_placeholderLabel.textColor"];
    [txtDryWeight setValue:[UIColor colorWithRed:110.0f/255.0f green:163.0f/255.0f blue:173.0f/255.0f alpha:1]
               forKeyPath:@"_placeholderLabel.textColor"];
    [txtConfirmPassword setValue:[UIColor colorWithRed:110.0f/255.0f green:163.0f/255.0f blue:173.0f/255.0f alpha:1]
                forKeyPath:@"_placeholderLabel.textColor"];
    [txtHospitalUsername setValue:[UIColor colorWithRed:110.0f/255.0f green:163.0f/255.0f blue:173.0f/255.0f alpha:1]
                      forKeyPath:@"_placeholderLabel.textColor"];
    [txtHospitalPassword setValue:[UIColor colorWithRed:110.0f/255.0f green:163.0f/255.0f blue:173.0f/255.0f alpha:1]
                      forKeyPath:@"_placeholderLabel.textColor"];
    [txtFirstName setValue:[UIColor colorWithRed:110.0f/255.0f green:163.0f/255.0f blue:173.0f/255.0f alpha:1]
                      forKeyPath:@"_placeholderLabel.textColor"];
    [txtLastName setValue:[UIColor colorWithRed:110.0f/255.0f green:163.0f/255.0f blue:173.0f/255.0f alpha:1]
                      forKeyPath:@"_placeholderLabel.textColor"];
    
        txtFirstName.autocapitalizationType = UITextAutocapitalizationTypeWords;
        txtLastName.autocapitalizationType = UITextAutocapitalizationTypeWords;
    
   //------ TextField Placeholder Color Change End Code //
    
    if([CommonFunctions reachabiltyCheck])
    {
        [CommonFunctions showActivityIndicatorWithText:@""];
    [self fetchHospitalData];
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Check Your Network Connection." withDelegate:self];
        flagforNoInternetAccess=true;
    }
    

    if([[UIScreen mainScreen]bounds].size.height<568)
    {
        [scrllVw setScrollEnabled:YES];
        [scrllVw setContentSize:CGSizeMake(320.0f, 500.0f)];
    }
    else
    {
        [scrllVw setScrollEnabled:NO];
    }
    
//Tap Gesture
    
    UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGestureClicked)];
    tapGesture.numberOfTapsRequired=1;
    tapGesture.delegate=self;
    [self.view addGestureRecognizer:tapGesture];

    txtSelectHospital.inputAccessoryView=toolBarView;
    txtDryWeight.inputAccessoryView=toolBarView;
    txtSelectHospital.inputView=viewPIcker;
    }

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    if(flagforScrollVwTxtDidBeginEditng)
    {
        [scrllVw setContentSize:CGSizeMake(320.0f,950.0f)];
    }
    else
    {
    [scrllVw setScrollEnabled:YES];
        [scrllVw setContentSize:CGSizeMake(320.0f, 750.0f)];
    }
}

#pragma mark - Custome SelectorMethods

-(void)tapGestureClicked
{
    [tfActiveTxt resignFirstResponder];
    [self scrollToNormalView];
    [scrllVw setScrollEnabled:YES];

}
-(void)leftButtonClicked
{
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}
#pragma mark - UIScrollView Methods

-(void)scrollViewToCenterOfScreen:(UITextField *)txtField
{
    float difference;
    
    if (scrllVw.contentSize.height == 600)
        difference = 50.0f;
    else
        difference = 60.0f;
    
    CGFloat viewCenterY = txtField.center.y;
    CGRect applicationFrame = [[UIScreen mainScreen] applicationFrame];
    CGFloat avaliableHeight = applicationFrame.size.height - 100.0f;
    CGFloat y = viewCenterY - avaliableHeight / 4.0f;
    
    if (y < 0)
        y = 0;
    
    [scrllVw setContentOffset:CGPointMake(0, y) animated:YES];
            [scrllVw setContentSize:CGSizeMake(320.0f, 1000.0f)];
}

-(void)scrollToNormalView
{
    if([[UIScreen mainScreen]bounds].size.height<568)
    {
        [scrllVw setScrollEnabled:YES];
        [scrllVw setContentSize:CGSizeMake(320.0f, 900.0f)];    }
    else
    {
        [scrllVw setContentSize:CGSizeMake(320.0f, 800.0f)];
        [scrllVw setContentOffset:CGPointZero];
    }
}

#pragma mark- UITextField Delegate methods

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    flagforScrollVwTxtDidBeginEditng=true;
    tfActiveTxt=textField;
    [textField setBackground:[UIImage imageNamed:@"activeTextBox"]];
    [textField setTextColor:[UIColor colorWithRed:37.0f/255.0f green:51.0f/255.0f blue:86.0f/255.0f alpha:1]];
    [textField setValue:[UIColor colorWithRed:37.0f/255.0f green:51.0f/255.0f blue:86.0f/255.0f alpha:1]
             forKeyPath:@"_placeholderLabel.textColor"];
    [scrllVw setScrollEnabled:YES];
    [self scrollViewToCenterOfScreen:textField];
    
    if(textField==txtSelectHospital || textField==txtHospitalUsername || textField==txtHospitalPassword )
    {
        flagForCMValidation=false;
    }
    else if (textField==txtUniqueID)
    {
        if(!flagForCMValidation)
        {
            if([self caseManagerValidate])
            {
                if([CommonFunctions reachabiltyCheck])
                {
                    [CommonFunctions showActivityIndicatorWithText:@""];
                    [self validateCMDetails];
                }
            }
        }
    }
    else
    {
        if(flagForCMValidation)
        {
            if(flagForMRNValidation == false)
            {
                if([self caseManagerValidate])
                {
                    if([self mrnNumberValidate])
                    {
                        [self.view endEditing:YES];
                        if([CommonFunctions reachabiltyCheck])
                        {
                            [CommonFunctions showActivityIndicatorWithText:@""];
                            [self validateMRN];
                        }
                    }
                }
            }
        }
        else
        {
            if([self caseManagerValidate])
            {
                [self mrnNumberValidate];
            }
            
        }
    }
   
    
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    flagforScrollVwTxtDidBeginEditng=false;
    [textField setBackground:[UIImage imageNamed:@"textBox"]];
    [textField setTextColor:[UIColor colorWithRed:110.0f/255.0f green:163.0f/255.0f blue:173.0f/255.0f alpha:1]];
    [textField setValue:[UIColor colorWithRed:110.0f/255.0f green:163.0f/255.0f blue:173.0f/255.0f alpha:1]
                      forKeyPath:@"_placeholderLabel.textColor"];
    
    
    if(textField==txtUniqueID)
    {
        flagForMRNValidation=false;
    }
    
    }

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField==txtHospitalUsername)
    {
        [txtHospitalPassword becomeFirstResponder];
    }
    else if (textField==txtHospitalPassword)
    {
        [txtUniqueID becomeFirstResponder];
    }
    else if (textField==txtUniqueID)
    {
        [txtDryWeight becomeFirstResponder];
    }
    else if(textField==txtDryWeight)
    {
        [txtFirstName becomeFirstResponder];
    }
    else if (textField==txtFirstName)
    {
        [txtLastName becomeFirstResponder];
    }
    else if (textField==txtLastName)
    {
        [txtUserName becomeFirstResponder];
    }
    else if(textField==txtUserName)
    {
        [txtPassword becomeFirstResponder];
    }
    else if(textField==txtPassword)
    {
        [txtConfirmPassword becomeFirstResponder];
    }
    else if(textField==txtConfirmPassword)
    {
        [textField resignFirstResponder];
        [self scrollToNormalView];
    }
    else if (flagforNoInternetAccess)
    {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    return YES;
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField==txtDryWeight)
    {
    NSString *editedString = [txtDryWeight.text stringByReplacingCharactersInRange:range withString:string];
    
    NSString    *regex     = @"\\d{0,3}(\\.\\d{0,2})?";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    
    return [predicate evaluateWithObject:editedString];
    }
    return YES;
}

#pragma mark- Custome Methods
-(BOOL)isValidate
{

    if(![CommonFunctions isValueNotEmpty:txtSelectHospital.text])
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please select hospital first." withDelegate:self withTag:100];
        
        return NO;
    }
    else if (![CommonFunctions isValueNotEmpty:txtHospitalUsername.text])
    {

        [CommonFunctions alertTitle:@"" withMessage:@"Please enter hospital username." withDelegate:self withTag:101];
        return NO;
    }
    else if (![CommonFunctions isValueNotEmpty:txtHospitalPassword.text])
    {

        [CommonFunctions alertTitle:@"" withMessage:@"Please enter hospital password." withDelegate:self withTag:102];
        return NO;
    }
    else if (![CommonFunctions isValueNotEmpty:txtUniqueID.text])
    {
  
        [CommonFunctions alertTitle:@"" withMessage:@"Please enter MRN no." withDelegate:self withTag:103];
        return NO;
    }

    else if (![CommonFunctions isValueNotEmpty:txtDryWeight.text])
    {

        [CommonFunctions alertTitle:@"" withMessage:@"Please enter dry weight." withDelegate:self withTag:104];
        return NO;
    }
    else if (![CommonFunctions isValueNotEmpty:txtFirstName.text])
    {

        [CommonFunctions alertTitle:@"" withMessage:@"Please enter first name." withDelegate:self withTag:105];
        return NO;
    }
    else if (![CommonFunctions isValueNotEmpty:txtLastName.text])
    {

        [CommonFunctions alertTitle:@"" withMessage:@"Please enter last name." withDelegate:self withTag:106];
        return NO;
    }
    else if (![CommonFunctions isValueNotEmpty:txtUserName.text])
    {

        [CommonFunctions alertTitle:@"" withMessage:@"Please enter Username." withDelegate:self withTag:107];
        return NO;
    }
    else if (![CommonFunctions isValueNotEmpty:txtPassword.text])
    {

        [CommonFunctions alertTitle:@"" withMessage:@"Please enter Password." withDelegate:self withTag:108];
        return NO;
    }
    else if (![CommonFunctions IsValidPassword:txtPassword.text])
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Password must have. \n 1. At least one upper case letter \n 2. At least one lower case letter. \n 3. At least one special character.  \n 4. At least one numeric value. \n 5. At least 8 character long."];
        return NO;
    }
    else if ([CommonFunctions trimSpaceInString:txtPassword.text].length<8)
    {

        [CommonFunctions alertTitle:@"" withMessage:@"Password must contain 8 or more characters." withDelegate:self withTag:109];
        return NO;
    }
    else if (![CommonFunctions isValueNotEmpty:txtConfirmPassword.text])
    {

        [CommonFunctions alertTitle:@"" withMessage:@"Please enter Confirm - Password first." withDelegate:self withTag:110];
        [txtConfirmPassword becomeFirstResponder];
        return NO;
    }
    else if (![txtPassword.text isEqualToString:txtConfirmPassword.text])
    {

        [txtPassword becomeFirstResponder];
        [CommonFunctions alertTitle:@"" withMessage:@"Passwords must match." withDelegate:self withTag:111];
        return NO;
    }
    return YES;
}

-(BOOL)caseManagerValidate
{

    if(![CommonFunctions isValueNotEmpty:txtSelectHospital.text])
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please enter hospital username." withDelegate:self withTag:120];

        
        [txtSelectHospital becomeFirstResponder];
        
        return NO;
    }
    else if (![CommonFunctions isValueNotEmpty:txtHospitalUsername.text])
    {

        [CommonFunctions alertTitle:@"" withMessage:@"Please enter hospital username." withDelegate:self withTag:121];
        [txtHospitalUsername becomeFirstResponder];
        return NO;
    }
    else if (![CommonFunctions isValueNotEmpty:txtHospitalPassword.text])
    {

        [CommonFunctions alertTitle:@"" withMessage:@"Please enter hospital password." withDelegate:self withTag:122];
                [txtHospitalPassword becomeFirstResponder];
        return NO;
    }
    return YES;
}

-(BOOL)mrnNumberValidate
{
    
    if(![CommonFunctions isValueNotEmpty:txtUniqueID.text])
    {

        [CommonFunctions alertTitle:@"" withMessage:@"Please enter MRN number first." withDelegate:self withTag:123];
        [txtUniqueID becomeFirstResponder];
        return NO;
    }
    return YES;
}


#pragma mark- IBAction Button

- (IBAction)saveNCreateBtnClicked:(id)sender {

    

    if([self isValidate])
    {
        [self.view endEditing:YES];
        [self scrollToNormalView];
        if ([CommonFunctions reachabiltyCheck]) {
            
            [CommonFunctions showActivityIndicatorWithText:@""];
            [self registerHospitalData];
        }
    }
}

#pragma mark- UIAlertView Delegate Methods
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==90) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if (alertView.tag==100)
    {
          [txtSelectHospital becomeFirstResponder];
    }
    else if (alertView.tag==101)
    {
        [txtHospitalUsername becomeFirstResponder];
    }
    else if (alertView.tag==102)
    {
        [txtHospitalPassword becomeFirstResponder];
    }
    else if (alertView.tag==103)
    {
        [txtUniqueID becomeFirstResponder];
    }
    else if (alertView.tag==104)
    {
        [txtDryWeight becomeFirstResponder];
    }
    else if (alertView.tag==105)
    {
        [txtFirstName becomeFirstResponder];
    }
    else if (alertView.tag==106)
    {
        [txtLastName becomeFirstResponder];
    }
    else if (alertView.tag==107)
    {
        [txtUserName becomeFirstResponder];
    }
    else if (alertView.tag==108)
    {
        [txtPassword becomeFirstResponder];
    txtPassword.text=@"";
        txtConfirmPassword.text=@"";
    }
    else if (alertView.tag==109)
    {
        [txtPassword becomeFirstResponder];
        txtPassword.text=@"";
        txtConfirmPassword.text=@"";
    }
    else if (alertView.tag==110)
    {
        [txtConfirmPassword becomeFirstResponder];

    }
    else if (alertView.tag==111)
    {
        [txtPassword becomeFirstResponder];
        txtPassword.text=@"";
        txtConfirmPassword.text=@"";
    }
    else if (alertView.tag==120)
    {
        [txtSelectHospital becomeFirstResponder];
    }
    else if (alertView.tag==121)
    {
        [txtHospitalUsername becomeFirstResponder];
    }
    else if (alertView.tag==122)
    {
        [txtHospitalPassword becomeFirstResponder];
    }
    else if (alertView.tag==123)
    {
        [txtUniqueID becomeFirstResponder];
    }
        else if (alertView.tag==212)
        {
            [txtHospitalUsername becomeFirstResponder];
        }

    [scrllVw setScrollEnabled:YES];
}
#pragma mark- UIPicker View Delegate Methods
// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if(tfActiveTxt==txtSelectHospital)
    {
        return arrHospital.count;
    }
    return 0;
}

// tell the picker the title for a given component
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *title;
    if (tfActiveTxt == txtSelectHospital)
    {
        title = [[arrHospital objectAtIndex:row] objectForKey:@"Name"];
        hospitalID=[[arrHospital objectAtIndex:row] objectForKey:@"HospitalId"];
    }
    return title;
}

- (void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (tfActiveTxt == txtSelectHospital)
    {
        if (arrHospital.count>0)
            txtSelectHospital.text = [[arrHospital objectAtIndex:row] objectForKey:@"Name"];
            hospitalID=[[arrHospital objectAtIndex:row] objectForKey:@"HospitalId"];
        
        if (![self isNotNull:slectHospitalPicker])
        {
            pickerValue = [[arrHospital objectAtIndex:row] objectForKey:@"Name"];
            hospitalID=[[arrHospital objectAtIndex:row] objectForKey:@"HospitalId"];
        }
    }
}

#pragma mark- UIBarButton Custome Action Methods

- (IBAction)pickerCategoryCancelClicked:(id)sender
{
    [tfActiveTxt resignFirstResponder];
    [self scrollToNormalView];
}

- (IBAction)pickerCategoryDoneClicked:(id)sender
{
    if (tfActiveTxt ==txtSelectHospital)
    {
        if(![CommonFunctions isValueNotEmpty:txtSelectHospital.text])
        {
            txtSelectHospital.text=[[arrHospital objectAtIndex:0] objectForKey:@"Name"];
            hospitalID=[[arrHospital objectAtIndex:0] objectForKey:@"HospitalId"];
            [txtHospitalUsername becomeFirstResponder];
        }
        else
        {
        [txtHospitalUsername becomeFirstResponder];
        }
    }
    else if (tfActiveTxt==txtDryWeight)
    {
        [txtFirstName becomeFirstResponder];
    }
    
}


#pragma mark- Webservice API
-(void)fetchHospitalData
{
    
   NSString *url = [NSString stringWithFormat:@"HospitalList"];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.152:100/api/Mobile/HospitalList
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeGet withHttpHeaders:nil withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])

        {
            arrHospital =[[NSMutableArray alloc]init];
            [arrHospital addObjectsFromArray:[responseDict objectForKey:@"replyData"]];
            
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            [self.navigationController popViewControllerAnimated:NO];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo%@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              
                                              
                                          }
                                          else{
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:@"Could not connect to the server." withDelegate:self withTag:90];
                                          }
                                          
                                      }];
}


-(void)registerHospitalData
{

    NSTimeZone *timeZone = [NSTimeZone localTimeZone];
    NSString *tzName = [timeZone name];

    NSString *strFName=[[CommonFunctions trimSpaceInString:txtFirstName.text] capitalizedString];

    NSString *strLName=[[CommonFunctions trimSpaceInString:txtLastName.text]capitalizedString];
    
    NSMutableDictionary *params=[[NSMutableDictionary alloc]initWithObjectsAndKeys:hospitalID,@"hospitalId",   [CommonFunctions trimSpaceInString:txtUniqueID.text],@"uniqueID",[CommonFunctions trimSpaceInString:txtDryWeight.text],@"dryWeight",[CommonFunctions trimSpaceInString:txtUserName.text],@"username",[CommonFunctions trimSpaceInString:txtConfirmPassword.text],@"password",strFName,@"firstname",tzName,@"timezone",strLName,@"lastname",[CommonFunctions trimSpaceInString:txtHospitalUsername.text],@"cm_username",[CommonFunctions trimSpaceInString:txtHospitalPassword.text],@"cm_password",pushDeviceToken,@"DeviceId", nil];
   // NSString *url = [NSString stringWithFormat:@"Registration"];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.152:99/api/Mobile/Registration

    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:nil withServiceName:@"Registration" withParameters:params withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
        {
            
            [[NSUserDefaults standardUserDefaults]setValue:[[responseDict objectForKey:@"replyData"]objectForKey:@"eventid"] forKey:UD_EVENT_ID];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            [[NSUserDefaults standardUserDefaults]setValue:[[responseDict objectForKey:@"replyData"]objectForKey:@"patientid"] forKey:UD_PATIENT_ID];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            [[NSUserDefaults standardUserDefaults]setValue:[[responseDict objectForKey:@"replyData"]objectForKey:@"fname"] forKey:UD_FIRST_NAME];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            [[NSUserDefaults standardUserDefaults]setValue:[[responseDict objectForKey:@"replyData"]objectForKey:UD_HOSPITAL_LOGO] forKey:UD_HOSPITAL_LOGO];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            [[NSUserDefaults standardUserDefaults]setValue:[[responseDict objectForKey:@"replyData"]objectForKey:@"day"] forKey:UD_DAY];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            NSLog(@"day : %@",[[NSUserDefaults standardUserDefaults]objectForKey:UD_EVENT_ID]);
            NSLog(@"token : %@",[[NSUserDefaults standardUserDefaults]objectForKey:UD_PATIENT_ID]);
            NSLog(@"token : %@",[[NSUserDefaults standardUserDefaults]objectForKey:UD_FIRST_NAME]);

            
            [APPDELEGATE allocTabBarControllar];
            [APPDELEGATE.window setRootViewController:APPDELEGATE.tabBar];
            [APPDELEGATE.window addSubview:APPDELEGATE.cstmTabBar];
            [APPDELEGATE.cstmTabBar setHidden:NO];
            [APPDELEGATE registerForRemoteNotification];

        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];

        }

        
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo%@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              }
                                          else{
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:@"Could not connect to the server."];
                                          }
                                          
                                      }];
}

-(void)validateCMDetails
{
    
    NSMutableDictionary *params=[[NSMutableDictionary alloc]initWithObjectsAndKeys:hospitalID,@"hospitalId",[CommonFunctions trimSpaceInString:txtHospitalUsername.text],@"cm_username",[CommonFunctions trimSpaceInString:txtHospitalPassword.text],@"cm_password", nil];
     NSString *url = [NSString stringWithFormat:@"validateCM"];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.152:99/api/Mobile/Registration
   // recoverlink.reliablesandbox.net/api/Mobile/Registration
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:nil withServiceName:url withParameters:params withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
        {
            flagForCMValidation=true;
        
            
        }
        else
        {
            if([[responseDict objectForKey:@"replyMsg"]isEqualToString:@"Case manager is not associate with seleted hospital."])
            {

              [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"] withDelegate:self withTag:212];
                [txtHospitalUsername becomeFirstResponder];
                
            }
            else if([[responseDict objectForKey:@"replyMsg"]isEqualToString:@"Case manager username or password is incorrect."])
            {

                [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"] withDelegate:self withTag:212];
                [txtHospitalUsername becomeFirstResponder];
                
            }
            else
            {
                [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"] ];
            }

            
        }
        
        
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo%@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:@"Could not connect to the server."];
                                          }
                                          
                                      }];
}

-(void)validateMRN
{
    
    NSMutableDictionary *params=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[CommonFunctions trimSpaceInString:txtUniqueID.text],@"mrn", nil];
    NSString *url = [NSString stringWithFormat:@"validateMRN"];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    // recoverlink.reliablesandbox.net/api/Mobile/validateMRN
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:nil withServiceName:url withParameters:params withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
        {
            flagForMRNValidation=true;

            [txtDryWeight becomeFirstResponder];
            
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:@"MRN Number Already Register."withDelegate:self];
            
            tfActiveTxt=txtUniqueID;

            
            
        }
        
        
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo%@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:@"Could not connect to the server."];
                                          }
                                          
                                      }];
}
@end
