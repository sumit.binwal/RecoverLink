//
//  WelcomeVC.h
//  RecoverLinks
//
//  Created by  on 24/02/15.
//  Copyright (c) 2015 . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WelcomeVC : UIViewController<UIGestureRecognizerDelegate,UITextFieldDelegate>
- (IBAction)submitButtonClicked:(id)sender;
- (IBAction)q4YesBtnClicked:(id)sender;
- (IBAction)q4NoBtnClicked:(id)sender;
- (IBAction)q3YesBtnClicked:(id)sender;
- (IBAction)q3NoBtnClicked:(id)sender;
- (IBAction)face1Clicked:(id)sender;
- (IBAction)face2Clicked:(id)sender;
- (IBAction)face3Clicked:(id)sender;
- (IBAction)face4Clicked:(id)sender;
- (IBAction)face5Clicked:(id)sender;



@end
