//
//  WelcomeVC.m
//  RecoverLinks
//
//  Created by  on 24/02/15.
//  Copyright (c) 2015 . All rights reserved.
//

#import "WelcomeVC.h"
#import "RegistrationVC.h"
#import "LoginVC.h"
#import "LearnVC.h"
#import "TipOfTheDayVC.h"
@interface WelcomeVC ()<UIAlertViewDelegate>
{
    
    IBOutlet UILabel *lblDay;
    IBOutlet UIScrollView *scrllVw;
    IBOutlet UIButton *navButton;
    
    IBOutlet UIView *navView;
    IBOutlet UILabel *lblName;
    IBOutlet UITextField *txtWeight;
    UITextField *activeTextFld;
    NSString *que3;
    NSString *que4;
    NSString *que1;
    IBOutlet UIView *accessaryVw;
    
    //Assign Flag Value to Alert View. When Click Question 2 Alert.. it BecomeFirstRespoder to Textfield
    BOOL isTextFiledValueInserted;

    //Assign Flag Value to Alrt View.. When User Sucessfully Done the Survay Alrt Index Going to TipofDay Page
    BOOL isSurvaySucessfullySubmited;
    
    IBOutlet UIButton *q4Yes;
    IBOutlet UIButton *q4No;
    IBOutlet UIButton *q3NoBtn;
    IBOutlet UIButton *q3YesBtn;
    IBOutlet UIButton *face5;
    IBOutlet UIButton *face4;
    IBOutlet UIButton *face3;
    IBOutlet UIButton *face2;
    IBOutlet UIButton *face1;
    
    NSDictionary *afterSubmitDataDict;
}

@end

@implementation WelcomeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
  
    [self setupView];
}

-(void)setupView
{
    [CommonFunctions setSecondNavigationBar:self.navigationController];
    
    //navView=[[UIView alloc]init];
    
    [navView setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width,50)];
    [self.navigationController.navigationBar addSubview:navView];
    [lblDay setText:[NSString stringWithFormat:@"%@/30",[[NSUserDefaults standardUserDefaults] objectForKey:UD_DAY]]];
    [lblName setText:[[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:UD_FIRST_NAME]] uppercaseString]];

    NSLog(@"%@",lblDay.text);
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:UD_DAY]);
    
    UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0,0, 20, 0)];
    txtWeight.rightView=view;
    txtWeight.rightViewMode=UITextFieldViewModeAlways;

    txtWeight.inputAccessoryView=accessaryVw;
    [self.navigationItem setHidesBackButton:YES animated:YES];

    
//Tap Gesture
    UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(singleTapClicked)];
    tapGesture.numberOfTapsRequired=1;
    tapGesture.delegate=self;
    [self.view addGestureRecognizer:tapGesture];
    [APPDELEGATE.cstmTabBar setHidden:NO];
    [APPDELEGATE.btnHome setBackgroundImage:[UIImage imageNamed:@"homeActive"] forState:UIControlStateNormal];
    que1=@"";
    que3=@"";
    que4=@"";
    txtWeight.text=@"";
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    
    if ([CommonFunctions reachabiltyCheck]) {
            [CommonFunctions showActivityIndicatorWithText:@""];
        [self getCurrentDay];
    }
    
    navView.hidden=NO;
    [lblDay setText:[NSString stringWithFormat:@"%@/30",[[NSUserDefaults standardUserDefaults] objectForKey:UD_DAY]]];
    [lblName setText:[[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:UD_FIRST_NAME]] uppercaseString]];
    [APPDELEGATE.btnSetting setBackgroundImage:[UIImage imageNamed:@"settingInactive"] forState:UIControlStateNormal];

}
-(void)viewDidLayoutSubviews
{
    [scrllVw setScrollEnabled:YES];
    [scrllVw setContentSize:CGSizeMake(320.0f, 800.0f)];
}

-(void)singleTapClicked
{
    [txtWeight resignFirstResponder];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [navView setHidden:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark-UITextField Delegate Methods
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeTextFld=textField;
[textField setBackground:[UIImage imageNamed:@"activeTextBox"]];
[textField setTextColor:[UIColor colorWithRed:37.0f/255.0f green:51.0f/255.0f blue:86.0f/255.0f alpha:1]];
[textField setValue:[UIColor colorWithRed:37.0f/255.0f green:51.0f/255.0f blue:86.0f/255.0f alpha:1]
         forKeyPath:@"_placeholderLabel.textColor"];

    [self scrollViewToCenterOfScreen:textField];
    [scrllVw setContentSize:CGSizeMake(320.0f, 1000.0f)];

}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField setBackground:[UIImage imageNamed:@"textBox"]];
    [textField setTextColor:[UIColor colorWithRed:110.0f/255.0f green:163.0f/255.0f blue:173.0f/255.0f alpha:1]];
    [textField setValue:[UIColor colorWithRed:110.0f/255.0f green:163.0f/255.0f blue:173.0f/255.0f alpha:1]
             forKeyPath:@"_placeholderLabel.textColor"];
    [scrllVw setContentSize:CGSizeMake(320.0f, 800.0f)];
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *editedString = [txtWeight.text stringByReplacingCharactersInRange:range withString:string];

    NSString    *regex     = @"\\d{0,3}(\\.\\d{0,2})?";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    
    return [predicate evaluateWithObject:editedString];
}

#pragma mark- ScrollVw Methods

-(void)scrollViewToCenterOfScreen:(UITextField *)txtField
{
    float difference;
    
    if (scrllVw.contentSize.height == 600)
        difference = 50.0f;
    else
        difference = 60.0f;
    
    CGFloat viewCenterY = txtField.center.y;
    CGRect applicationFrame = [[UIScreen mainScreen] applicationFrame];
    CGFloat avaliableHeight = applicationFrame.size.height - 100.0f;
    CGFloat y = viewCenterY - avaliableHeight / 4.0f;
    
    if (y < 0)
        y = 0;
    
    [scrllVw setContentOffset:CGPointMake(0, y) animated:YES];
    [scrllVw setContentSize:CGSizeMake(320.0f, 1000.0f)];
}


#pragma mark- IBAction Methods
- (IBAction)faceBtnClicked:(id)sender {
        [self.view endEditing:YES];
    UIButton *btn=(UIButton *)sender;
    int btntag=(int)btn.tag;
    
    [face1 setImage:[UIImage imageNamed:@"face1"] forState:UIControlStateNormal];
    [face2 setImage:[UIImage imageNamed:@"face2"] forState:UIControlStateNormal];
    [face3 setImage:[UIImage imageNamed:@"face3"] forState:UIControlStateNormal];
    [face4 setImage:[UIImage imageNamed:@"face4"] forState:UIControlStateNormal];
    [face5 setImage:[UIImage imageNamed:@"face5"] forState:UIControlStateNormal];
    
    switch (btntag) {
        case 1:
            [btn setImage:[UIImage imageNamed:@"activeFace1"] forState:UIControlStateNormal];
            que1=@"1";
            break;
        case 2:
            [btn setImage:[UIImage imageNamed:@"activeFace2"] forState:UIControlStateNormal];
            que1=@"2";
            break;
        case 3:
            [btn setImage:[UIImage imageNamed:@"activeFace3"] forState:UIControlStateNormal];
            que1=@"3";
            break;
        case 4:
            [btn setImage:[UIImage imageNamed:@"activeFace4"] forState:UIControlStateNormal];
            que1=@"4";
            break;
        case 5:
            [btn setImage:[UIImage imageNamed:@"activeFace5"] forState:UIControlStateNormal];
            que1=@"5";
            break;
            }
    
}

- (IBAction)submitButtonClicked:(id)sender {
        [self.view endEditing:YES];
    if([self surveyValidation])
    {

        if([CommonFunctions reachabiltyCheck])
        {
            [CommonFunctions showActivityIndicatorWithText:@""];
            [self sendSurvayInfo];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:@"Check Your Network Connection."];
        }
    }
    else
    {
        NSLog(@"fjasd;] ");
    }
}

- (IBAction)q4YesBtnClicked:(id)sender {
        [self.view endEditing:YES];
    [q4No setImage:[UIImage imageNamed:@"noBtn"] forState:UIControlStateNormal];
    [q4Yes setImage:[UIImage imageNamed:@"activeYes"] forState:UIControlStateNormal];

    que4=@"true";
}

- (IBAction)q4NoBtnClicked:(id)sender {
        [self.view endEditing:YES];
    [q4No setImage:[UIImage imageNamed:@"activeNo"] forState:UIControlStateNormal];
    [q4Yes setImage:[UIImage imageNamed:@"yesBtn"] forState:UIControlStateNormal];
    
    que4=@"false";
}

- (IBAction)question3BtnClicked:(id)sender {
    [self.view endEditing:YES];
    UIButton *btn=(UIButton *)sender;
    int btntag=(int)btn.tag;
    [q3NoBtn setImage:[UIImage imageNamed:@"noBtn"] forState:UIControlStateNormal];
    [q3YesBtn setImage:[UIImage imageNamed:@"yesBtn"] forState:UIControlStateNormal];
    switch (btntag) {
        case 1:
            [btn setImage:[UIImage imageNamed:@"activeYes"] forState:UIControlStateNormal];
            que3=@"true";
            break;
        case 2:
            [btn setImage:[UIImage imageNamed:@"activeNo"] forState:UIControlStateNormal];
            que3=@"false";
            break;
    }
}

#pragma mark -Registeration  WebService..
-(void)sendSurvayInfo
{
    NSTimeZone *timeZone = [NSTimeZone localTimeZone];
    NSString *tzName = [timeZone name];
    
    
    NSString *survaySubmitDay=[[NSUserDefaults standardUserDefaults]objectForKey:UD_DAY];

    NSString *date=[NSString stringWithFormat:@"%@",[NSDate date]];

    NSMutableDictionary *params=[[NSMutableDictionary alloc]initWithObjectsAndKeys:survaySubmitDay,@"DayNo",date,@"Created",que1,@"QuestionOne",txtWeight.text,@"QuestonTwo", que3,@"QuestionThree", que4,@"QuestionFour",[[NSUserDefaults standardUserDefaults]objectForKey:UD_EVENT_ID],@"eventID",tzName,@"timezone",nil];
     NSString *url = [NSString stringWithFormat:@"DailySurvey"];

    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.152:100/api/Mobile/DailySurvey
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:nil withServiceName:url withParameters:params withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
        {
            [CommonFunctions alertTitle:@"" withMessage:@"Your survey was submitted successfully. If you are having a health emergency call 911." withDelegate:self];
            afterSubmitDataDict=[responseDict objectForKey:@"replyData"];
            isSurvaySucessfullySubmited=true;
          //  [self performSelectorOnMainThread:@selector(tipOfTheDayView:) withObject:responseDict waitUntilDone:YES];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo%@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:@"Could not connect to the server."];
                                          }
                                      }];
}

-(void)tipOfTheDayView:(NSDictionary*)dict
{

    TipOfTheDayVC *tod=[[TipOfTheDayVC alloc]init];
  
     if ([[dict objectForKey:@"typeOftip"]  isEqualToString:@"Url"]) {
            tod.videoUrl=[NSString stringWithFormat:@"%@",[dict objectForKey:@"Url"] ];
            tod.tipofDay=[NSString stringWithFormat:@"%@",[dict objectForKey:@"tipOfDay"] ];
        }
    else
        {
            if([self isNotNull:[dict objectForKey:@"tipOfDay"]])
            {
                tod.tipofDay=[NSString stringWithFormat:@"%@",[dict objectForKey:@"tipOfDay"]];
            }
            tod.tipofDay=[NSString stringWithFormat:@"%@",[dict objectForKey:@"tipOfDay"]] ;
            if (            [self isNotNull:[dict objectForKey:@"findmore"]])
            {
            [[NSUserDefaults standardUserDefaults] setObject:[dict objectForKey:@"findmore"] forKey:UD_FIND_MORE];
            }
            
            [[NSUserDefaults standardUserDefaults]synchronize];
        }
        NSLog(@"tip of day : %@",tod.tipofDay);
        NSLog(@"%@",[dict objectForKey:@"tipOfDay"] );
    
        [self.navigationController pushViewController:tod animated:YES];
    
       [[UIApplication sharedApplication]cancelAllLocalNotifications];
       [APPDELEGATE registerForRemoteNotificationNextDay];
    
    
    
        NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
        [allViewControllers removeObjectIdenticalTo:self];
        NSLog(@"%@",allViewControllers);
        self.navigationController.viewControllers = [NSArray arrayWithArray:(NSArray *)allViewControllers];
}


#pragma mark - Survey Question Validation

-(BOOL)surveyValidation
{
    BOOL isSucess=NO;
    if(![self isNotNull:que1]&&![self isNotNull:txtWeight.text]&&![self isNotNull:que3]&&![self isNotNull:que4])
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please answer your survey questions to let your case manager know how you're doing." withDelegate:self];
        return isSucess;
    }
        return isSucess=YES;
}

-(IBAction)doneBtnClicked:(id)sender
{
    [txtWeight resignFirstResponder];
}
-(IBAction)cancleButtnClicked:(id)sender
{
    [txtWeight resignFirstResponder];
}

#pragma mark- UIAlert View Delegate Methods

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
if(isSurvaySucessfullySubmited)
{
    [self tipOfTheDayView:afterSubmitDataDict];
}
}



-(void)getCurrentDay
{
    NSTimeZone *timeZone = [NSTimeZone localTimeZone];
    NSString *tzName = [timeZone name];
    
    NSString *eventID=[[NSUserDefaults standardUserDefaults]objectForKey:UD_EVENT_ID];
    NSString *url = [NSString stringWithFormat:@"getDay?Id=%@&timezone=%@",eventID,tzName];
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.152:100/api/Mobile/getDay?Id=2033&timezone=-3:00
    
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeGet withHttpHeaders:nil withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSMutableDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
        {

            [self performSelectorOnMainThread:@selector(getActualDayView:) withObject:responseDict waitUntilDone:YES];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo%@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                          }
                                          else{
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:@"Could not connect to the server."];
                                          }
                                      }];
}

-(void)getActualDayView:(NSMutableDictionary*)dict
{
    NSLog(@"%@",[[dict objectForKey:@"replyData"] objectForKey:@"SetDailySurvey"]);

    
    [[NSUserDefaults standardUserDefaults]setValue:[[dict objectForKey:@"replyData"]objectForKey:@"dayNo"] forKey:UD_DAY];
    [[NSUserDefaults standardUserDefaults]setValue:[[dict objectForKey:@"replyData"]objectForKey:@"Status"] forKey:UD_STATUS];
    if([self isNotNull:[[dict objectForKey:@"replyData"]objectForKey:@"findmore"]])
    {
        [[NSUserDefaults standardUserDefaults]setValue:[[dict objectForKey:@"replyData"]objectForKey:@"findmore"] forKey:UD_FIND_MORE];
    }
    
    
    
    [[NSUserDefaults standardUserDefaults]setValue:[[dict objectForKey:@"replyData"]objectForKey:@"SetDailySurvey"] forKey:UD_SURVAY];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    NSString *str=[[[dict objectForKey:@"replyData"] objectForKey:@"SetDailySurvey"] stringValue];
    if([str isEqualToString:@"1"])
    {
        [[UIApplication sharedApplication]cancelAllLocalNotifications];
        [APPDELEGATE registerForRemoteNotificationNextDay];
    }
    else
    {
        [APPDELEGATE registerForRemoteNotification];
    }
    
    
    
    if([[[dict objectForKey:@"replyData"] objectForKey:@"dayNo"]intValue]<=30)
            {
                if ([[[dict objectForKey:@"replyData"]objectForKey:@"Status"]isEqualToString:@"Disenrolled"]||[[[dict objectForKey:@"replyData"]objectForKey:@"Status"]isEqualToString:@"Deceased"]||[[[dict objectForKey:@"replyData"]objectForKey:@"Status"]isEqualToString:@"Completed 30-days"])
                {
                    [APPDELEGATE allocTabBarAfter30Days];
    
                    APPDELEGATE.setUpTabBar;
                    APPDELEGATE.tabBar.selectedIndex=0;
                    [APPDELEGATE.window addSubview:APPDELEGATE.cstmTabBar];
                    [APPDELEGATE.cstmTabBar setHidden:NO];
                    [[UIApplication sharedApplication] cancelAllLocalNotifications];
                    [[UIApplication sharedApplication]cancelLocalNotification:APPDELEGATE.notification];
                    [APPDELEGATE.btnHome setBackgroundImage:[UIImage imageNamed:@"homeActive"] forState:UIControlStateNormal];
        
                }
                else
                {
                    if([[[[dict objectForKey:@"replyData"] objectForKey:@"SetDailySurvey"] stringValue] isEqualToString:@"1"])
                    {
                        
                        TipOfTheDayVC *tvc=[[TipOfTheDayVC alloc]initWithNibName:@"TipOfTheDayVC" bundle:nil];
                        [APPDELEGATE allocTabBarTipControllar:tvc];
                        APPDELEGATE.setUpTabBar;
                        APPDELEGATE.tabBar.selectedIndex=0;
                        [APPDELEGATE.window addSubview:APPDELEGATE.cstmTabBar];
                        [APPDELEGATE.cstmTabBar setHidden:NO];
                        [[UIApplication sharedApplication] cancelAllLocalNotifications];
                        [APPDELEGATE.btnHome setBackgroundImage:[UIImage imageNamed:@"homeActive"] forState:UIControlStateNormal];
                        
                        if ([[[dict objectForKey:@"replyData"] objectForKey:@"typeOftip"] isEqualToString:@"Url"]) {
                            tvc.videoUrl=[NSString stringWithFormat:@"%@",[[dict objectForKey:@"replyData"] objectForKey:@"Url"]];
                            NSLog(@"%@",tvc.videoUrl);
                            tvc.tipofDay=[NSString stringWithFormat:@"%@",[[dict objectForKey:@"replyData"] objectForKey:@"tipOfDay"]];
                        }
                        else
                        {
                            if([self isNotNull:[[dict objectForKey:@"replyData"] objectForKey:@"tipOfDay"]])
                            {
                                tvc.tipofDay=[NSString stringWithFormat:@"%@",[[dict objectForKey:@"replyData"] objectForKey:@"tipOfDay"] ];
                            }
                        }
                        
                        
                    }
                    else
                    {
                       
                        [APPDELEGATE registerForRemoteNotification];
                        
                        [face1 setImage:[UIImage imageNamed:@"face1"] forState:UIControlStateNormal];
                        [face2 setImage:[UIImage imageNamed:@"face2"] forState:UIControlStateNormal];
                        [face3 setImage:[UIImage imageNamed:@"face3"] forState:UIControlStateNormal];
                        [face4 setImage:[UIImage imageNamed:@"face4"] forState:UIControlStateNormal];
                        [face5 setImage:[UIImage imageNamed:@"face5"] forState:UIControlStateNormal];
                        
                        [q3NoBtn setImage:[UIImage imageNamed:@"noBtn"] forState:UIControlStateNormal];
                        [q4No setImage:[UIImage imageNamed:@"noBtn"] forState:UIControlStateNormal];

                        [q4Yes setImage:[UIImage imageNamed:@"yesBtn"] forState:UIControlStateNormal];
                        [q3NoBtn setImage:[UIImage imageNamed:@"noBtn"] forState:UIControlStateNormal];

                        que1=@"";
                        txtWeight.text=@"";
                        que3=@"";
                        que4=@"";
                        
                        [lblDay setText:[NSString stringWithFormat:@"%@/30",[[NSUserDefaults standardUserDefaults] objectForKey:UD_DAY]]];
                        [lblName setText:[[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:UD_FIRST_NAME]] uppercaseString]];
                        
                        
                        

                    }
                }
            }
        else
        {
            [APPDELEGATE allocTabBarAfter30Days];
    
            APPDELEGATE.setUpTabBar;
            APPDELEGATE.tabBar.selectedIndex=0;
            [APPDELEGATE.window addSubview:APPDELEGATE.cstmTabBar];
            [APPDELEGATE.cstmTabBar setHidden:NO];
            [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
            [[UIApplication sharedApplication] cancelAllLocalNotifications];
            [[UIApplication sharedApplication]cancelLocalNotification:APPDELEGATE.notification];

            [APPDELEGATE.btnHome setBackgroundImage:[UIImage imageNamed:@"homeActive"] forState:UIControlStateNormal];
            
            
        }

    

    
}


@end
