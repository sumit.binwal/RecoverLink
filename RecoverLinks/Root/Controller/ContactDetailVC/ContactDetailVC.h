//
//  ContactDetailVC.h
//  RecoverLinks
//
//  Created by  on 12/03/15.
//  Copyright (c) 2015 . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactDetailVC : UIViewController
{
    NSMutableDictionary *dataDict;
}
- (IBAction)callButtonClicked:(id)sender;
@property (nonatomic,strong)    NSMutableDictionary *dataDict;
@end
