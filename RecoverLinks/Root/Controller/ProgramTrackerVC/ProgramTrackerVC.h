//
//  ProgramTrackerVC.h
//  RecoverLinks
//
//  Created by  on 31/03/15.
//  Copyright (c) 2015 . All rights reserved.
//

#import <UIKit/UIKit.h>



@interface ProgramTrackerVC : UIViewController

@property(nonatomic,strong)IBOutlet UICollectionView *BreathCollectionView;
@property(nonatomic,strong)IBOutlet UICollectionView *AppCollectionView;
@property(nonatomic,strong)IBOutlet UICollectionView *ArmsCollectionView;


-(void)setUpView;
-(void)callPropgrameTrackerAPI;

@end
