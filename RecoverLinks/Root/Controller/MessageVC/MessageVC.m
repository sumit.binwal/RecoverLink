//
//  MessageVC.m
//  RecoverLinks
//
//  Created by  on 11/03/15.
//  Copyright (c) 2015 . All rights reserved.
//

#import "MessageVC.h"

@interface MessageVC ()
{
    IBOutlet UIImageView *imgLineView1;
    IBOutlet UIImageView *imgLineView;
    IBOutlet UILabel *lblCMMTime;
    IBOutlet UILabel *lblCMMsg;
    IBOutlet UILabel *lblMessage;
    IBOutlet UIScrollView *scrollVw;
    IBOutlet UILabel *lblReason;
    IBOutlet UILabel *lblTime;
    BOOL flagForMsgRead;
}
@end

@implementation MessageVC
@synthesize dataDict,srvrDate,messageID;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    [self setUpView];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    if ([CommonFunctions reachabiltyCheck]) {
        [CommonFunctions showActivityIndicatorWithText:@""];
        [self getMessageDetail];
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Please check your network connection first."];
    }
}
-(void)setUpView
{
    [CommonFunctions setNavigationBar:self.navigationController];
    [self.navigationItem setTitle:@"MESSAGES"];
    UIBarButtonItem *leftBtn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"backArrow"] style:UIBarButtonItemStylePlain target:self action:@selector(leftBarButtonClicked)];
    [self.navigationItem setLeftBarButtonItem:leftBtn];
        leftBtn.imageInsets = UIEdgeInsetsMake(2, -5, 0, 0);

 
    
}

-(void)leftBarButtonClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(NSString *)dateForString:(NSString *)serverDate
{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSDate *date = [dateFormatter dateFromString:serverDate];
    
//    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSYearCalendarUnit|NSDayCalendarUnit|NSMonthCalendarUnit|NSHourCalendarUnit|NSMinuteCalendarUnit|NSSecondCalendarUnit fromDate:date];
//    
//    
//    
//    NSDateComponents *dateComponent = [[NSDateComponents alloc] init];
//    dateComponent.day = components.day;
//    dateComponent.year = components.year;
//    dateComponent.month = components.month;
//    
//    NSDate *apiDate = [[NSCalendar currentCalendar] dateFromComponents:dateComponent];
//    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
//    NSDate *defaultDate = [dateFormatter dateFromString:[dateFormatter stringFromDate:[NSDate date]]];
//    
//    //if ([apiDate compare:defaultDate]==NSOrderedSame) {
//        
//        NSDateComponents *comp1 = [[NSDateComponents alloc] init];
//        comp1.hour = components.hour;
//        comp1.minute = components.minute;
//        NSDate *onlyTime = [[NSCalendar currentCalendar] dateFromComponents:comp1];
//        [dateFormatter setDateFormat:@"MMM dd, hh:mm a"];
//        [dateFormatter stringFromDate:onlyTime];
//        NSString *srvrDateString;
//        srvrDateString=[NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:onlyTime]];
//        return srvrDateString;
//    }
//    else
//    {
    [dateFormatter setDateFormat:@"MMM dd, hh:mm a"];
    NSString *srvrDateString;
    [dateFormatter stringFromDate:date];
    srvrDateString=[NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:date]];
    return srvrDateString;
//    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- WebServices API..

-(void)msgIsReadOnDevice
{
    NSTimeZone *timeZone = [NSTimeZone localTimeZone];
    NSString *tzName = [timeZone name];

    NSMutableDictionary *params=[[NSMutableDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:UD_EVENT_ID],@"EventId",[dataDict objectForKey:@"replyid"],@"MessageId",@"1",@"IsRead",tzName,@"timezone",nil];
    
    NSString *url = [NSString stringWithFormat:@"DeviceMessageRead"];
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.152:100/api/Mobile/DeviceMessageRead/
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:nil withServiceName:url withParameters:params withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {
            flagForMsgRead=true;
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            [self.navigationController popViewControllerAnimated:NO];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo%@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              
                                              
                                          }
                                          else{
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:@"Could not connect to the server."];
                                          }
                                          
                                      }];
}

#pragma mark- WebServices API..

-(void)getMessageDetail
{
    NSTimeZone *timeZone = [NSTimeZone localTimeZone];
    NSString *tzName = [timeZone name];
    
    NSString *url = [NSString stringWithFormat:@"getMessage/?Id=%@&msgId=%@&timezone=%@",[[NSUserDefaults standardUserDefaults] objectForKey:UD_EVENT_ID],messageID,tzName];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://recoverlink.reliablesandbox.net/api/Mobile/getMessage/?Id=131&msgId=438&timezone=+13
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeGet withHttpHeaders:nil withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {
            [imgLineView setHidden:NO];
            [imgLineView1 setHidden:NO];
            dataDict =[[responseDict objectForKey:@"replyData"] objectAtIndex:0];
            NSLog(@"%@",dataDict);
                    NSString *str=[NSString stringWithFormat:@"%@",[dataDict objectForKey:@"isread"]];
            if(![self isNotNull:[dataDict objectForKey:@"reply"]])
            {
                lblReason.text=[dataDict objectForKey:@"messagetype"];
                lblMessage.text=[dataDict objectForKey:@"message"];
                lblTime.text=[self dateForString:[dataDict objectForKey:@"messagedate"]];
                lblCMMsg.text=@"Awaiting response";
                [lblCMMsg setFont:[UIFont fontWithName:FONT_SEGOE_BOLD size:17.0f]];
                [lblCMMsg setTextColor:[UIColor colorWithRed:158.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:1]];
                
                lblCMMTime.text=@"";
//                UIBarButtonItem *rgtBtn=[[UIBarButtonItem alloc]initWithTitle:[NSString stringWithFormat:@"%@",[self dateForString:[dataDict objectForKey:@"messagedate"]]] style:UIBarButtonItemStylePlain target:self action:nil];
//                [self.navigationItem setRightBarButtonItem:rgtBtn];
            }
                    else
                    {
                        lblReason.text=[dataDict objectForKey:@"messagetype"];
                        lblMessage.text=[dataDict objectForKey:@"message"];
                        lblTime.text=[self dateForString:[dataDict objectForKey:@"messagedate"]];
                        if([self isNotNull:[dataDict objectForKey:@"reply"]])
                        {
                            lblCMMsg.text=[dataDict objectForKey:@"reply"];
                            lblCMMTime.text=[self dateForString:[dataDict objectForKey:@"replydate"]];
//                            UIBarButtonItem *rgtBtn=[[UIBarButtonItem alloc]initWithTitle:[NSString stringWithFormat:@"%@",[self dateForString:[dataDict objectForKey:@"replydate"]]] style:UIBarButtonItemStylePlain target:self action:nil];
//                            [self.navigationItem setRightBarButtonItem:rgtBtn];
                            if([[[dataDict objectForKey:@"isReplyRead"]stringValue]isEqualToString:@"0"])
                            {
                                if ([CommonFunctions reachabiltyCheck])
                                {
                                    [CommonFunctions showActivityIndicatorWithText:@""];
                                    [self msgIsReadOnDevice];
                                }
                            }
                            
//                            [lblCMMsg sizeToFit];
//                            [lblMessage sizeToFit];
//                            [scrollVw setContentSize:CGSizeMake([UIScreen mainScreen].bounds.size.width, lblCMMsg.frame.size.height+lblCMMsg.frame.origin.y+200+lblMessage.frame.size.height)];
//                            NSLog(@"------ %f",lblCMMsg.frame.size.height);
//                            [scrollVw setScrollEnabled:YES];
                            
                        }
                        else
                        {
                            lblCMMsg.text=@"Awaiting response";
                            [lblCMMsg setFont:[UIFont fontWithName:FONT_SEGOE_BOLD size:17.0f]];
                            [lblCMMsg setTextColor:[UIColor colorWithRed:158.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:1]];
                            
                            lblCMMTime.text=@"";
//                            UIBarButtonItem *rgtBtn=[[UIBarButtonItem alloc]initWithTitle:[NSString stringWithFormat:@"%@",[self dateForString:[dataDict objectForKey:@"messagedate"]]] style:UIBarButtonItemStylePlain target:self action:nil];
//                            [self.navigationItem setRightBarButtonItem:rgtBtn];
                        }
                    }
                
            
            [lblCMMsg sizeToFit];
            [lblMessage sizeToFit];
            [scrollVw setContentSize:CGSizeMake([UIScreen mainScreen].bounds.size.width, lblCMMsg.frame.size.height+lblCMMsg.frame.origin.y+200+lblMessage.frame.size.height)];
            NSLog(@"------ %f",lblCMMsg.frame.size.height);
            [scrollVw setScrollEnabled:YES];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            [self.navigationController popViewControllerAnimated:NO];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo%@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              
                                              
                                          }
                                          else{
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:@"Could not connect to the server."];
                                          }
                                          
                                      }];
}

@end
