//
//  ProgCompleteVC.h
//  RecoverLinks
//
//  Created by  on 13/04/15.
//  Copyright (c) 2015 . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProgCompleteVC : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *lblHeading;
@property (strong, nonatomic) IBOutlet UILabel *lblDetail;

@end
