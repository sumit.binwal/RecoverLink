//
//  ContactListVC.m
//  RecoverLinks
//
//  Created by  on 12/03/15.
//  Copyright (c) 2015 . All rights reserved.
//

#import "ContactListVC.h"
#import "ContactListCustomCell.h"
#import "ContactDetailVC.h"
@interface ContactListVC ()<UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate>
{
    IBOutlet UILabel *lblError;
    NSMutableArray *contactListArray;
    IBOutlet UITableView *tblVw;
}
@end

@implementation ContactListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [CommonFunctions setNavigationBar:self.navigationController];
    [self.navigationItem setTitle:@"CONTACTS"];
    
    UIBarButtonItem *leftBtn=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"backArrow"] style:UIBarButtonItemStylePlain target:self action:@selector(leftBarButtonClicked)];
    [self.navigationItem setLeftBarButtonItem:leftBtn];
    leftBtn.imageInsets = UIEdgeInsetsMake(2, -5, 0, 0);
    
    if([CommonFunctions reachabiltyCheck])
    {
        [CommonFunctions showActivityIndicatorWithText:@""];
        [self getContactList];
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Check your Network Connection."];
    }
    // Do any additional setup after loading the view from its nib.
    
}
-(void)leftBarButtonClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    if([CommonFunctions reachabiltyCheck])
    {
        [CommonFunctions showActivityIndicatorWithText:@""];
        [self getContactList];
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"Check your Network Connection."];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark-UITableView Delegate Methods.
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70.0f;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectiontableview
{
    return contactListArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"cellIdentifier";
    ContactListCustomCell *cell=[[ContactListCustomCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    if(cell==nil)
    {
        cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        
    }

    NSArray *paths = [tableView indexPathsForVisibleRows];
    
    if (contactListArray.count == paths.count) {
        tableView.scrollEnabled=NO;
    }
    else
    {
        tableView.scrollEnabled=YES;
    }
    
    
    if(indexPath.row==0)
    {
        [cell.imgDevider setHidden:NO];
    }
    else
    {
        [cell.imgDevider setHidden:YES];
    }
    if(contactListArray.count>0)
    {
    cell.lblTitle.text=[[[NSString stringWithFormat:@"%@ %@",[[contactListArray objectAtIndex:indexPath.row] objectForKey:@"FirstName"],[[contactListArray objectAtIndex:indexPath.row] objectForKey:@"LastName"]] lowercaseString] capitalizedString];
    cell.lblPost.text=[[[[contactListArray objectAtIndex:indexPath.row] objectForKey:@"Title"] lowercaseString] capitalizedString];
    [cell.callBtn addTarget:self action:@selector(getCallButton:) forControlEvents:UIControlEventTouchUpInside];
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:@"No Contact Found." withDelegate:self];
    }
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ContactDetailVC *cdvc=[[ContactDetailVC alloc]initWithNibName:@"ContactDetailVC" bundle:nil];
    [self.navigationController pushViewController:cdvc animated:YES];
    cdvc.dataDict=[contactListArray objectAtIndex:indexPath.row];
    NSLog(@"%@",cdvc.dataDict);
}

-(void)getCallButton:(UIButton *)sender
{
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:tblVw];
    NSIndexPath *indexPath = [tblVw indexPathForRowAtPoint:buttonPosition];

    
        NSString *phoneNumber = [@"tel://" stringByAppendingString:[CommonFunctions trimSpaceInString:[[contactListArray objectAtIndex:indexPath.row]objectForKey:@"Phone"]]];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];

}


#pragma mark-Webservice Calling Methods
-(void)getContactList
{
    NSString *url = [NSString stringWithFormat:@"Contact/%@",[[NSUserDefaults standardUserDefaults] objectForKey:UD_EVENT_ID]];
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    //http://192.168.0.152:100/api/Mobile/Contact/
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypeGet withHttpHeaders:nil withServiceName:url withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        NSLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:@"Server Error"];
            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
            
        {
            contactListArray =[[NSMutableArray alloc]init];
            [contactListArray addObjectsFromArray:[responseDict objectForKey:@"replyData"]];
            if(contactListArray.count>0)
            {
            [tblVw reloadData];
            }
            else
            {
                [lblError setHidden:NO];
            }

        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            [self.navigationController popViewControllerAnimated:NO];
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              NSLog(@"impo%@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              
                                              
                                          }
                                          else{
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:@"Could not connect to the server."];
                                          }
                                          
                                      }];
}

#pragma mark- UIAlert View Delegate Methods

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
