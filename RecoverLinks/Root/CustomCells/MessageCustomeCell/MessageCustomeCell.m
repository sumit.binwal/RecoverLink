//
//  MessageCustomeCell.m
//  RecoverLinks
//
//  Created by  on 13/03/15.
//  Copyright (c) 2015 . All rights reserved.
//

#import "MessageCustomeCell.h"

@interface MessageCustomeCell ()

@end

@implementation MessageCustomeCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self=[[[NSBundle mainBundle]loadNibNamed:@"MessageCustomeCell" owner:self options:nil] objectAtIndex:0];
    return self;
}
@end
