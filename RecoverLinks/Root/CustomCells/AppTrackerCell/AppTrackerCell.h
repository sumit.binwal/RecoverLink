//
//  AppTrackerCell.h
//  RecoverLinks
//
//  Created by  on 31/03/15.
//  Copyright (c) 2015 . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppTrackerCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgCross;
@property (strong, nonatomic) IBOutlet UIImageView *imgGraphBar;

@end
