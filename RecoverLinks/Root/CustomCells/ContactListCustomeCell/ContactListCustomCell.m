//
//  ContactListCustomCell.m
//  RecoverLinks
//
//  Created by  on 12/03/15.
//  Copyright (c) 2015 . All rights reserved.
//

#import "ContactListCustomCell.h"

@interface ContactListCustomCell ()

@end

@implementation ContactListCustomCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    
    self=[[[NSBundle mainBundle]loadNibNamed:@"ContactListCustomCell" owner:self options:nil] objectAtIndex:0];
    return self;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
