//
//  MoodTrackerCell.h
//  GraphAPI
//
//  Created by  on 31/03/15.
//  Copyright (c) 2015   Infosolutions Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MoodTrackerCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *face5;
@property (strong, nonatomic) IBOutlet UIImageView *face1;
@property (strong, nonatomic) IBOutlet UIImageView *face2;
@property (strong, nonatomic) IBOutlet UIImageView *face4;
@property (strong, nonatomic) IBOutlet UIImageView *face3;

@end
